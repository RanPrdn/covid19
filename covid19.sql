-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 13 Apr 2020 pada 06.34
-- Versi server: 10.4.6-MariaDB
-- Versi PHP: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `covid19`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `penyaluran`
--

CREATE TABLE `penyaluran` (
  `id_penyaluran` int(11) NOT NULL,
  `id_terima` int(11) NOT NULL,
  `tgl_penyaluran` date NOT NULL,
  `skpd` varchar(300) NOT NULL,
  `penerima` varchar(200) NOT NULL,
  `nama_barang` text NOT NULL,
  `jumlah` int(11) NOT NULL,
  `satuan_salur` varchar(100) NOT NULL,
  `ket` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `penyaluran`
--

INSERT INTO `penyaluran` (`id_penyaluran`, `id_terima`, `tgl_penyaluran`, `skpd`, `penerima`, `nama_barang`, `jumlah`, `satuan_salur`, `ket`) VALUES
(8, 6, '2020-04-11', ' DINAS KESEHATAN ', 'NURLIYASMAN', '200', 150, 'pCS', 'q');

-- --------------------------------------------------------

--
-- Struktur dari tabel `rekap`
--

CREATE TABLE `rekap` (
  `id_rekap` int(11) NOT NULL,
  `tgl_terima` date NOT NULL,
  `pemberi` text NOT NULL,
  `nama_barang` text NOT NULL,
  `jenis_barang` text NOT NULL,
  `volume` int(11) NOT NULL,
  `satuan_pemberi` text NOT NULL,
  `harga_satuan` int(11) NOT NULL,
  `nilai_barang` int(11) NOT NULL,
  `tgl_salur` date NOT NULL,
  `skpd_penerima` text NOT NULL,
  `penerima` text NOT NULL,
  `jumlah` int(11) NOT NULL,
  `satuan_penyalur` text NOT NULL,
  `sisa_barang` int(11) NOT NULL,
  `ket` text NOT NULL,
  `bast` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `terima`
--

CREATE TABLE `terima` (
  `id_terima` int(11) NOT NULL,
  `tgl_terima` date NOT NULL,
  `nama_pemberi` text NOT NULL,
  `jabatan` varchar(200) NOT NULL,
  `alamat` text NOT NULL,
  `nama_barang` text NOT NULL,
  `jenis_barang` varchar(100) NOT NULL,
  `volume_beri` int(11) NOT NULL,
  `satuan_beri` varchar(50) NOT NULL,
  `hrga_satuan` int(11) NOT NULL,
  `nilai_barang` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `terima`
--

INSERT INTO `terima` (`id_terima`, `tgl_terima`, `nama_pemberi`, `jabatan`, `alamat`, `nama_barang`, `jenis_barang`, `volume_beri`, `satuan_beri`, `hrga_satuan`, `nilai_barang`) VALUES
(4, '2020-03-21', 'Bapak Eddy Hussy (Tim Koordinator Donasi Melawan COVID-19 Kota Batam )', '', '', 'Franklin Real-Time PCR Thermocycler (alat labor)', 'MEDIS', 1, 'Set', 0, 0),
(5, '2020-03-21', 'Bapak Eddy Hussy (Tim Koordinator Donasi Melawan COVID-19 Kota Batam )', '', '', 'SARS-Cov-2 Go-Strips', 'MEDIS', 30, 'Pcs', 0, 0),
(6, '2020-04-09', 'BPD HiPMI KEPRI ', '', '', 'Masker Kain', 'NON MEDIS', 200, 'pCS', 10000, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `pwd` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id_user`, `username`, `pwd`) VALUES
(1, 'ardi', '202cb962ac59075b964b07152d234b70');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `penyaluran`
--
ALTER TABLE `penyaluran`
  ADD PRIMARY KEY (`id_penyaluran`);

--
-- Indeks untuk tabel `rekap`
--
ALTER TABLE `rekap`
  ADD PRIMARY KEY (`id_rekap`);

--
-- Indeks untuk tabel `terima`
--
ALTER TABLE `terima`
  ADD PRIMARY KEY (`id_terima`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `penyaluran`
--
ALTER TABLE `penyaluran`
  MODIFY `id_penyaluran` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `rekap`
--
ALTER TABLE `rekap`
  MODIFY `id_rekap` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `terima`
--
ALTER TABLE `terima`
  MODIFY `id_terima` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
