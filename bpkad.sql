-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 24 Okt 2019 pada 03.06
-- Versi server: 10.4.6-MariaDB
-- Versi PHP: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bpkad`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `anggaran`
--

CREATE TABLE `anggaran` (
  `id_anggaran` int(10) NOT NULL,
  `jdl_anggaran` varchar(100) COLLATE utf8_bin NOT NULL,
  `uraian` text COLLATE utf8_bin NOT NULL,
  `tahun` varchar(10) COLLATE utf8_bin NOT NULL,
  `file` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data untuk tabel `anggaran`
--

INSERT INTO `anggaran` (`id_anggaran`, `jdl_anggaran`, `uraian`, `tahun`, `file`) VALUES
(3, 'JANUARI', 'LAPORAN REALISASI ANGGARAN PENDAPATAN DAN BELANJA UNTUK TAHUN YANG BERAKHIR SAMPAI JANUARI 2019', '2019', '1_januari.pdf'),
(4, 'FEBRUARI', 'LAPORAN REALISASI ANGGARAN PENDAPATAN DAN BELANJA UNTUK TAHUN YANG BERAKHIR SAMPAI FEBRUARI 2019', '2019', '2__februari.pdf'),
(5, 'MARET', 'LAPORAN REALISASI ANGGARAN PENDAPATAN DAN BELANJA UNTUK TAHUN YANG BERAKHIR SAMPAI MARET 2019', '2019', '3__maret.pdf'),
(6, 'APRIL', 'LAPORAN REALISASI ANGGARAN PENDAPATAN DAN BELANJA UNTUK TAHUN YANG BERAKHIR SAMPAI APRIL 2019', '2019', '4__april.pdf'),
(7, 'MEI', 'LAPORAN REALISASI ANGGARAN PENDAPATAN DAN BELANJA UNTUK TAHUN YANG BERAKHIR SAMPAI MEI 2019', '2019', '5__mei.pdf'),
(8, 'JUNI', 'LAPORAN REALISASI ANGGARAN PENDAPATAN DAN BELANJA UNTUK TAHUN YANG BERAKHIR SAMPAI JUNI 2019', '2019', '6__juni.pdf'),
(9, 'JULI', 'LAPORAN REALISASI ANGGARAN PENDAPATAN DAN BELANJA UNTUK TAHUN YANG BERAKHIR SAMPAI 2019', '2019', '7__juli.pdf'),
(10, 'AGUSTUS', 'LAPORAN REALISASI ANGGARAN PENDAPATAN DAN BELANJA UNTUK TAHUN YANG BERAKHIR SAMPAI AGUSTUS 2019', '2019', '8__agustus.pdf'),
(11, 'SEPTEMBER', 'LAPORAN REALISASI ANGGARAN PENDAPATAN DAN BELANJA UNTUK TAHUN YANG BERAKHIR SAMPAI SEPTEMBER 2019', '2019', '9_september.pdf');

-- --------------------------------------------------------

--
-- Struktur dari tabel `apbd`
--

CREATE TABLE `apbd` (
  `id_apbd` int(11) NOT NULL,
  `jdl_apbd` varchar(100) COLLATE utf8_bin NOT NULL,
  `uraian` text COLLATE utf8_bin NOT NULL,
  `tahun` varchar(10) COLLATE utf8_bin NOT NULL,
  `file` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Struktur dari tabel `brt_bpkad`
--

CREATE TABLE `brt_bpkad` (
  `id_brt` bigint(100) NOT NULL,
  `id_ktg_brt` int(10) NOT NULL,
  `id_user` int(10) NOT NULL,
  `nm_brt` varchar(50) COLLATE utf8_bin NOT NULL,
  `smr_brt` varchar(30) COLLATE utf8_bin NOT NULL,
  `tgl_pst` date NOT NULL,
  `isi_brt` text COLLATE utf8_bin NOT NULL,
  `foto_brt` text COLLATE utf8_bin NOT NULL,
  `status_brt` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data untuk tabel `brt_bpkad`
--

INSERT INTO `brt_bpkad` (`id_brt`, `id_ktg_brt`, `id_user`, `nm_brt`, `smr_brt`, `tgl_pst`, `isi_brt`, `foto_brt`, `status_brt`) VALUES
(1, 1, 1, 'ASA', 'AS', '2019-09-12', 'ADADB', 'BPKAD', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `foto_kegiatan`
--

CREATE TABLE `foto_kegiatan` (
  `id_ft_kegiatan` int(11) NOT NULL,
  `jdl_kegiatan` text NOT NULL,
  `keterangan` text NOT NULL,
  `foto` text NOT NULL,
  `foto2` text NOT NULL,
  `foto3` text NOT NULL,
  `status` varchar(10) NOT NULL,
  `tgl_kegiatan` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `foto_kegiatan`
--

INSERT INTO `foto_kegiatan` (`id_ft_kegiatan`, `jdl_kegiatan`, `keterangan`, `foto`, `foto2`, `foto3`, `status`, `tgl_kegiatan`) VALUES
(15, 'KUNJUNGAN KERJA SEKRETARIAT DAERAH KABUPATEN MUSI BANYUASIN', 'Kasubbid Pengeluaran Belanja Daerah, Kasubbid Inventarisasi dan Pelaporan Aset Daerah, dan Kasubbid Akuntansi Pendapatan Daerah menerima Kunjungan Sekretariat Daerah Kabupaten Musi Banyuasin bersama rombongan dalam rangka studi mekanisme transaksi non tunai dan peraturan KDH penatausahaan persediaan.', '033c98ff-e548-48d4-baea-a26441da8a81.JPG', '26b08942-bc8f-43a4-ae15-0bd2ec413904.JPG', 'ddd911f0-4a0b-42ee-b0ac-e9bcfef676f6.JPG', 'Yes', '2019-09-17'),
(18, 'PEMERIKSAAN FISIK PELANTAR BETON PULAU MECAN', 'Pemeriksaan fisik pelantar beton Pulau Mecan, Kelurahaan Sekanak Raya Kecamatan Belakang Padang. Oleh Bidang Aset (Kasubbid Inventarisasi dan Pelaporan Aset dan Staf) dan Pengurus Barang Pengguna Dinas Perikanan Kota Batam dan tim dalam rangka menindaklanjuti permohonan reklasifikasi aset di lingkungan Dinas Perikanan Kota Batam.', '2ea63164-4080-41f3-ac87-c4c1baaeac04.jpg', 'd35846af-18af-4468-b1c0-aebd647cad5e.jpg', 'db6f0c58-087f-41d8-8e8b-4acda9b19cc8.jpg', 'Yes', '2019-09-12'),
(19, 'GOTONG ROYONG DI MASJID SULTAN MAHMUD RIAYAT SYAH', 'Kepala Badan beserta staf BPKAD Kota Batam ikut serta dalam Acara Gotong Royong Pembersihan Masjid Sultan Mahmud Riayat Syah.', 'IMG-4691.JPG', 'IMG-4684.JPG', 'IMG-4686.JPG', 'Yes', '2019-09-18'),
(20, 'KUNJUNGAN KERJA KOMISI II DPRD KOTA PALEMBANG', ' Kepala Bidang Aset Menerima Kunjungan Kerja dari Komisi II DPRD Kota Batam.', '784953c5-156b-4b0f-9f3a-21ba1345ba97.JPG ', 'a286bc37-30b3-4421-a0dc-27f5362ac7cb.JPG ', 'gambar.jpeg', 'Yes', '2019-09-24'),
(21, 'UPACARA PERINGATAN HARI JADI PROVINSI KEPULAUAN RIAU', 'Upacara Peringatan Hari Jadi Provinsi Kepulauan Riau ke 17 Tingkat Kota Batam Tahun 2019. Pembina Upacara : Amsakar Achmad, S.Sos, M.Si (Wakil Walikota Batam) Perwira Upacara : Drs. Faisal Riza, M.Ec.Dev (Sekretaris BPKAD Kota Batam)', 'ec2ffe7d-948d-4237-8ffa-07bbbf106ea9.JPG', 'upacara1.JPG', 'upacara.jpeg', 'Yes', '2019-09-27'),
(22, 'PENANDATANGANAN NASKAH PERJANJIAN HIBAH DAERAH (NPHD)', 'Penandatanganan Naskah Perjanjian Hibah Daerah (NPHD) Antara Pemerintah Kota Batam Dengan Komisi Pemilihan Umum (KPU) Dan Badan Pengawas Pemilihan Umum (Bawaslu) Tahun Anggaran 2019-2020', 'gambar2.jpg', 'gambar.jpg', 'gambar3.jpg', 'Yes', '2019-10-01'),
(24, 'Kunjungan kerja Badan Keuangan dan Aset Daerah (BKAD) Kota Manado', 'Kunjungan kerja Badan Keuangan dan Aset Daerah (BKAD) kota manado, yang dilaksanakan di gedung kantor walikota Batam pada tanggal 2 Oktober 2019 hari Rabu', 'rapat_1.jpeg', 'rapat_2.jpeg', 'rapat_3.jpeg', 'No', '2019-10-02'),
(25, 'Rapat koordinasi terkait konfirmasi Pengelolaan Gedung Promosi Se Sumatra.', 'Rapat koordinasi terkait konfirmasi Pengelolaan Gedung Promosi Se Sumatra.', 'WhatsApp_Image_2019-10-07_at_19_17_061.jpeg', 'WhatsApp_Image_2019-10-07_at_19_17_051.jpeg', 'WhatsApp_Image_2019-10-07_at_19_17_06(1)1.jpeg', 'Yes', '2019-10-14'),
(26, 'Sekretaris BPKAD menerima Kunjungan Kerja BPKAD Tulungagung', 'Sekretaris BPKAD Kota Batam Drs. Faisal Riza, M.Ec.Dev menerima Kunjungan Kerja dari BPKAD Tulungagung perihal pengelolaan keuangan dan aset.', 'w2.jpg', 'w1.jpg', 'w3.jpg', 'Yes', '2019-10-14'),
(27, 'Penyerahan Piagam WTP ke Walikota Batam oleh Kanwil Dirjen Perbendaharaan Provinsi Kepulauan Riau.', 'FGD/Sharing Session bersama OPD Pengelolaan Dana DAK Fisik sekaligus Penyerahan Piagam WTP ke Walikota Batam oleh Kanwil Dirjen Perbendaharaan Provinsi Kepulauan Riau.  Rabu, 9 Oktober 2019', 'eee.jpg', 'rrr.jpg', 'www.jpg', 'Yes', '2019-10-14'),
(28, 'Kunjungan Kerja dari BPKAD Pemerintah Kota Palembang, terkait Pembelajaran Tentang Pengamanan dan Pemanfaatan Aset.', 'Kasubbid Perencanaan dan Analisa Kebutuhan Aset, Kasubbid Inventarisasi dan Pelaporan Aset Daerah dan Kasubbid Penilian dan Pemanfaatan Aset Menerima Kunjungan Kerja dari BPKAD Pemerintah Kota Palembang, terkait Pembelajaran Tentang Pengamanan dan Pemanfaatan Aset.  14 Oktober 2019', 'w31.jpg', 'w21.jpg', 'w11.jpg', 'Yes', '2019-10-16'),
(29, 'Perihal Study Teknisi Implementasi Peningkatan Kapasitas Pengelolaan BMD OPD lingkup Pemerintah Provinsi Sumatera Barat.', 'Kabid Perbendaharaan, Kasubbid Inventarisasi dan Pelaporan Aset Daerah dan Kasubbid Peniliaan dan Pemanfaatan Aset Menerima Kunjungan dari Sekretariat Daerah Provinsi Sumatera Barat Perihal Study Teknisi Implementasi Peningkatan Kapasitas Pengelolaan BMD OPD lingkup Pemerintah Provinsi Sumatera Barat.  15 Oktober 2019', 'w22.jpg', 'w12.jpg', 'w32.jpg', 'Yes', '2019-10-16'),
(30, 'Studi Banding Pemerintah Daerah Provinsi Kepulauan Bangka Belitung', 'Studi Banding Pemerintah Daerah Provinsi Kepulauan Bangka Belitung ke Pemerintah Kota Batam terkait pengaturan dalam pembelian Jenis Bahan Bakar Minyak Tertentu (Solar Bersubsidi) dengan menggunakan Uang Elektronik (Non Tunai).', 'w33.jpg', 'w4.jpg', 'w23.jpg', 'Yes', '2019-10-16');

-- --------------------------------------------------------

--
-- Struktur dari tabel `hibah`
--

CREATE TABLE `hibah` (
  `id_hibah` int(11) NOT NULL,
  `jdl_hibah` varchar(100) COLLATE utf8_bin NOT NULL,
  `uraian` text COLLATE utf8_bin NOT NULL,
  `file` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Struktur dari tabel `kebijakan_bpkad`
--

CREATE TABLE `kebijakan_bpkad` (
  `id_kebijakan` int(2) NOT NULL,
  `isi_kebijakan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kebijakan_bpkad`
--

INSERT INTO `kebijakan_bpkad` (`id_kebijakan`, `isi_kebijakan`) VALUES
(1, '<li>Meningkatkan kelancaran pelayanan administrasi, kualitas sumber daya aparatur sesuai dengan tugas dan fungsinya.</li>'),
(2, '<li>Meningkatkan pengelolaan perencanaan, pelaksanaan keuangan daerah yang mendukung kebutuhan pembangunan daerah.</li>'),
(3, '<li>Meningkatkan pengelolaan penerimaan dan pengeluaran keuangan daerah sesuai dengan ketentuan yang berlaku.</li>'),
(4, '<li>Meningkatkan pembinaan kepada OPD bersama instansi terkait dalam rangka mewujudkan Sistem Akuntasi Pemerintah berbasis Akrual yang tepat waktu.</li>'),
(5, '<li>Meningkatkan penyelenggaran sistem informasi pengelolaan aset daerah dan terintegrasinya sistem pengelolaan keuangan dan aset daerah.</li>');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kegiatan_bpkad`
--

CREATE TABLE `kegiatan_bpkad` (
  `id_kegiatan_bpkad` bigint(100) NOT NULL,
  `id_ktg_kegiatan` int(10) NOT NULL,
  `id_user` int(10) NOT NULL,
  `nm_kegiatan` varchar(50) COLLATE utf8_bin NOT NULL,
  `ft_kegiatan` varchar(100) COLLATE utf8_bin NOT NULL,
  `tgl_pst` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Struktur dari tabel `keuda`
--

CREATE TABLE `keuda` (
  `id_keuda` int(11) NOT NULL,
  `ktg_keuda` varchar(100) NOT NULL,
  `nm_keuda` varchar(100) NOT NULL,
  `uraian_keuda` text NOT NULL,
  `tahun` varchar(10) NOT NULL,
  `file` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `keuda`
--

INSERT INTO `keuda` (`id_keuda`, `ktg_keuda`, `nm_keuda`, `uraian_keuda`, `tahun`, `file`) VALUES
(1, 'Peraturan Daerah', 'No 9 tahun 2016', 'Tentang Perubahan Anggaran Pendapatan dan Belanja Daerah Kota Batam Tahun 2016', '2016', 'Peraturan_Daerah_Kota_Batam_No_9_Tahun_2016.pdf'),
(2, 'Peraturan Daerah', 'No 3 Tahun 2016', 'Tentang Anggaran Pendapatan dan Belanja Daerah Kota Batam Tahun 2016', '2016', 'Peraturan_Daerah_Kota_Batam_No__3_Tahun_2016.pdf'),
(3, 'Peraturan Daerah', 'No 1 Tahun 2010', 'Tentang Urusan Pemerintah Yang Menjadi Kewenangan Pemerintah Daerah', '2010', 'Peraturan_Daerah_Kota_Batam_No__1_Tahun_2010.pdf'),
(4, 'Peraturan Pemerintah', 'No 2 Tahun 2012', 'Tentang Hibah Daerah', '2012', 'PP_NO__2_TAHUN_2012.pdf'),
(5, 'Peraturan Pemerintah', 'No 8 Tahun 2006', 'Pelaporan Keuangan dan Kinerja Instansi Pemerintah', '2016', 'PP_NO__8_TAHUN_2006_-_PELAPORAN_KEUANGAN_DAN_KINERJA_INSTANSI_PEMERINTAH.pdf'),
(6, 'Peraturan Pemerintah', 'No 12 Tahun 2019', 'Pengelolaan Keuangan Daerah', '2019', 'PP_NO__12_TAHUN_2019_-_PENGELOLAAN_KEUANGAN_DAERAH.pdf'),
(7, 'Peraturan Pemerintah', 'No 27 Tahun 2014', 'Tentang Pengelolaan Barang Milik Negara Atau Daerah', '2014', 'PP_NO__27_TAHUN_2014_-_TENTANG_PENGELOLAAN_BARANG_MILIK_NEGARA_ATAU_DAERAH.pdf'),
(8, 'Peraturan Pemerintah', 'No 54 Tahun 2005', 'Pinjaman Daerah', '2005', 'PP_NO__54_TAHUN_2005_-_PINJAMAN_DAERAH.pdf'),
(9, 'Peraturan Pemerintah', 'No 55 Tahun 2005', 'Dana Perimbangan', '2005', 'PP_NO__55_TAHUN_2005_-_DANA_PERIMBANGAN.pdf'),
(10, 'Peraturan Pemerintah', 'No 56 Tahun 2005', 'Sistem Informasi Keuangan Daerah', '2005', 'PP_NO__56_TAHUN_2005_-_SISTEM_INFORMASI_KEUANGAN_DAERAH.pdf'),
(11, 'Peraturan Pemerintah', 'No 71 Tahun 2010', 'Standart Akuntansi Pemerintah', '2010', 'PP_NO__71_TAHUN_2010_-_STANDAR_AKUNTANSI_PEMERINTAHAN.pdf'),
(12, 'Peraturan Pemerintah', 'No 74 Tahun 2012', 'Perubahan Atas Peraturan Pemerintah No 23 Tahun 2005 Tentang Pengelolaan Keuangan Badan Layanan Umum', '2012', 'PP_NO__74_TAHUN_2012_-_PERUBAHAN_ATAS_PERATURAN_PEMERINTAH_NOMOR_23_TAHUN_2005_TENTANG_PENGELOLAAN_KEUANGAN_BADAN_LAYANAN_UMUM.pdf'),
(13, 'Peraturan Pemerintah', 'No 32 Tahun 2014', 'Tentang Pengelolaan dan Pemanfaatan dana Kapitasi Jaminan Kesehatan Nasional pada Fasilitas Kesehatan Tingkat Pertama Milik Pemerintah Daerah', '2014', 'Peraturan_Presiden_No_32_Tahun_2014_(Tentang_Pengelolaan_dan_Pemanfaatan_dana_Kapitasi_Jaminan_Kesehatan_Nasional_pada_Fasilitas_Kesehatan_Tingkat_Pertama_Milik_Pemerintah_Daerah).pdf'),
(14, 'Undang Undang', 'No 33 Tahun 2004', 'Tentang Perimbangan Keuangan Antara Pemerintah Pusat dan Pemerintahan Daerah', '2004', 'Undang_-_Undang_Nomor_33_Tahun_2004(Tentang_Perimbangan_Keuangan_Pemerintah_Pusat_dan_Pemerintahan_Daerah).pdf'),
(19, 'Undang Undang', 'No 1 Tahun 2004', 'Tentang Perbendaharaan Negara', '2004', 'Undang_Undang_Nomor_1_Tahun_2004_(Tentang_Perbendaharaan_Negara).pdf'),
(20, 'Undang Undang', 'No 17 Tahun 2003', 'Tentang Keuangan Negara', '2003', 'Undang_Undang_Nomor_17_Tahun_2003_(Tentang_Keuangan_Negara).pdf'),
(21, 'Undang Undang', 'No 15 Tahun 2004', 'Pemeriksaan Pengelolaan dan Pertanggung Jawaban Keuangan Negara', '2004', 'UU_NO__15_TAHUN_2004_-_PEMERIKSAAN_PENGELOLAAN_DAN_TANGGUNG_JAWAB_KEUANGAN_NEGARA.pdf'),
(22, 'Peraturan Presiden', 'No 32 Tahun 2014', 'Tentang Pengelolaan dan Pemanfaatan Dana Kapitasi Jaminan Kesehatan Nasional pada Fasilitas Kesehatan Tingkat Pertama Milik Pemerintah Daerah', '2014', 'Peraturan_Presiden_No_32_Tahun_2014_(Tentang_Pengelolaan_dan_Pemanfaatan_dana_Kapitasi_Jaminan_Kesehatan_Nasional_pada_Fasilitas_Kesehatan_Tingkat_Pertama_Milik_Pemerintah_Daerah).pdf'),
(23, 'Peraturan Walikota', 'No 20 Tahun 2019', 'Tata Cara Pemberian dan Pertanggung Jawaban Belanja Tak Terduga', '2019', 'PERWAKO_NO__20_THN_2019_(TATA_CARA_PEMBERIAN_DAN_PERTANGGUNGJAWABAB_BELANJA_TAK_TERDUGA).pdf'),
(24, 'Peraturan Walikota', 'No 09 Tahun 2019', 'Perubahan Atas Perwarko Batam No 40 Tahun Tentang Tata Cara Pemberian dan Pertanggung Jawaban Hibah dan Bantuan Sosial', '2019', 'PERWAKO_NO_9_TH_2019_(PERUBAHAN_ATAS_PERWAKO_BTM_NO__40_THN_2018_TTG_TATA_CARA_PEMBERIAN_DAN_PERTANGGUNGJAWABAN_HIBAH_DAN_BANSOS).pdf'),
(25, 'Peraturan Walikota', 'No 28 Tahun 2019', 'Perubahan Kedua Atas Peraturan Walikota Batam No 40 Tahun 2018 Tentang Tata Cara Pemberian Dan Pertanggung Jawaban Hibah dan Bantuan Sosial', '2019', 'PERWAKO_NO_28_THN_2019_(PERUBAHAN_KEDUA_ATAS_PERATURAN_WALIKOTA_BATAM_NOMOR_40_THN_2018_TTG_TATA_CARA_PEMBERIAN_DABN_PERTANGGUNGJAWABAN_HIBAH_DAN_BANSOS).pdf'),
(26, 'Peraturan Walikota', 'No 40 Tahun 2018', 'Tata Cara Pemberian dan Pertanggung Jawaban Hibah dan Bantuan Sosial', '2018', 'PERWAKO_No_40_TAHUN_2018_TATA_CARA_PEMBERIAN_DAN_PERTANGGUNGJAWABAN_HIBAH_DAN_BANTUAN_SOSIAL.pdf'),
(27, 'Undang Undang', 'No 23 Tahun 2014', 'Tentang Pemerintahan Daerah', '2014', 'UU_Nomor_23_Tahun_2014.pdf'),
(28, 'Peraturan Menteri Dalam Negeri', 'No 130 Tahun 2018', 'Kegiatan Pembangunan Sarana dan Prasaran Kelurahahan dan Pemberdayaan Masyarakat di Kelurahan', '2018', 'KEMENTERIAN_DALAM_NEGER_130_TAHUN_2018_-_KEGIATAN_PEMBANGUNAN_SARANA_DAN_PRASARANA_KELURAHAN_DAN_PEMBERDAYAAN_MASYARAKAT_DI_KELURAHAN.pdf'),
(32, 'Peraturan Menteri Dalam Negeri', 'No 32 Tahun 2012 ', 'Pedoman Penyusunan, Pengendalian dan Evaluasi Rencana Kerja Pembangunan Daerah Tahun 2013', '2012', 'KEMENTERIAN_DALAM_NEGERI_32_TAHUN_2012_-_PEDOMAN_PENYUSUNAN,_PENGENDALIAN_DAN_EVALUASI_RENCANA_KERJA_PEMBANGUNAN_DAERAH_TAHUN_20131.pdf'),
(33, 'Peraturan Menteri Dalam Negeri', 'No 14 Tahun 2016 ', 'Perubahan Kedua Atas Peraturan Menteri Dalam Negeri Republik Indonesia No 32 Tahun 2011 Tentang Pedoman Pemberian Hibah Dan Bantuan Sosial Yang Bersumber Dari Anggaran Pendapatan Dan Belanja Daerah', '2016', 'KEMENTERIAN_DALAM_NEGERI_14_TAHUN_2016_-_KEGIATAN_PEMBANGUNAN_SARANA_DAN_PRASARANA_KELURAHAN_DAN_PEMBERDAYAAN_MASYARAKAT_DI_KELURAHAN.pdf'),
(34, 'Peraturan Menteri Dalam Negeri', 'No 32 Tahun 2011', ' Pedoman Pemberian Hibah dan Bantuan Sosial Yang bersumber dari Anggaran Pendapatan dan Belanja Daerah', '2011', 'Peraturan_Menteri_Dalam_Negeri_No_32_Tahun_2011_(Pedoman_Pemberian_Hibah_dan_Bantuan_Sosial_Yang_bersumber_dari_Anggaran_Pendapatan_dan_Belanja_Daerah).pdf'),
(35, 'Peraturan Menteri Dalam Negeri', 'No 64 Tahun 2013 ', 'Tentang Penerapan Standar Akuntansi Pemerintahan Berbasis Aktual Pada Pemerintah Daerah', '2013', 'Peraturan_Menteri_Dalam_Negeri_No_64_Tahun_2013_(Tentang_Penerapan_Standar_Akuntansi_Pemerintahan_Berbasis_Akrual_Pada_Pemerintah_Daerah).pdf'),
(36, 'Peraturan Menteri Dalam Negeri', 'No 13 Tahun 2006 ', 'Tentang Pedoman Pengelolaan Keuangan Daerah', '2006', 'Peraturan_Menteri_Dalam_Negeri_No__13_Tahun_2006_(Tentang_Pedoman_Pengelolaan_Keuangan_Daerah).pdf'),
(37, 'Peraturan Daerah', 'No 6 Tahun 2018 ', 'Tentang Anggaran Pendapatan dan Belanja Daerah ', '2018', 'Perda_Btm_2018_No_06_APBD.pdf'),
(38, 'Peraturan Daerah', 'No 3 Tahun 2014', 'Tentang Penyertaan Modal Daerah Pemerintah Kota  Batam Pada Beberapa Badan Usaha Milik Daerah', '2014', 'Perda_PernyataanModalBtm_2014_No_3.pdf');

-- --------------------------------------------------------

--
-- Struktur dari tabel `ktg_brt`
--

CREATE TABLE `ktg_brt` (
  `id_ktg_brt` int(10) NOT NULL,
  `nm_ktg` varchar(50) COLLATE utf8_bin NOT NULL,
  `keterangan` varchar(100) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data untuk tabel `ktg_brt`
--

INSERT INTO `ktg_brt` (`id_ktg_brt`, `nm_ktg`, `keterangan`) VALUES
(1, 'informasi', 'berbagi informasi');

-- --------------------------------------------------------

--
-- Struktur dari tabel `ktg_kegiatan`
--

CREATE TABLE `ktg_kegiatan` (
  `id_ktg_kegiatan` int(10) NOT NULL,
  `nm_kegiatan` varchar(50) COLLATE utf8_bin NOT NULL,
  `ket_kegiatan` varchar(100) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Struktur dari tabel `lkpd`
--

CREATE TABLE `lkpd` (
  `id_lkpd` int(11) NOT NULL,
  `jdl_lkpd` varchar(100) COLLATE utf8_bin NOT NULL,
  `uraian` text COLLATE utf8_bin NOT NULL,
  `bagian` varchar(100) COLLATE utf8_bin NOT NULL,
  `file` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data untuk tabel `lkpd`
--

INSERT INTO `lkpd` (`id_lkpd`, `jdl_lkpd`, `uraian`, `bagian`, `file`) VALUES
(1, 'LK UN AUDIT', 'LK UN AUDIT', 'LK UN AUDIT', 'QWE'),
(2, 'LK AUDIT', 'LK AUDIT', 'LK AUDIT', 'ASD'),
(3, 'OPINI BPK', 'OPINI BPK', 'OPINI BPK', 'TYU');

-- --------------------------------------------------------

--
-- Struktur dari tabel `lkpj`
--

CREATE TABLE `lkpj` (
  `id_lkpj` int(11) NOT NULL,
  `jdl_lkpj` varchar(100) COLLATE utf8_bin NOT NULL,
  `uraian` text COLLATE utf8_bin NOT NULL,
  `bagian` varchar(100) COLLATE utf8_bin NOT NULL,
  `file` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data untuk tabel `lkpj`
--

INSERT INTO `lkpj` (`id_lkpj`, `jdl_lkpj`, `uraian`, `bagian`, `file`) VALUES
(1, 'Akhir Tahun Anggaran', 'Akhir Tahun Anggaran', 'Akhir Tahun Anggaran', 'asd'),
(2, 'Akhir Masa Jabatan', 'Akhir Masa Jabatan', 'Akhir Masa Jabatan', 'qwe');

-- --------------------------------------------------------

--
-- Struktur dari tabel `misi_bpkad`
--

CREATE TABLE `misi_bpkad` (
  `id_misi` int(2) NOT NULL,
  `isi_misi` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `misi_bpkad`
--

INSERT INTO `misi_bpkad` (`id_misi`, `isi_misi`) VALUES
(1, '<li>Meningkatkan pelayanan administrasi, akuntabilitas kinerja dan keuangan serta profesionalisme sumber daya aparatur.</li>'),
(2, '<li>Meningkatkan sistem pengelolaan Anggaran Pendapatan dan Belanja Daerah yang transparan dan profesional.</li>'),
(3, '<li>Meningkatkan layanan penatausahaan yang akuntabel.</li>'),
(4, '<li>Meningkatkan kualitas penyusunan dan penyajian laporan keuangan daerah berdasarkan Standar Akuntansi Pemerintah berbasis Akrual.</li>'),
(5, '<li>Meningkatkan penataan dan pengelolaan aset daerah yang efektif dan efisien.</li>');

-- --------------------------------------------------------

--
-- Struktur dari tabel `profile`
--

CREATE TABLE `profile` (
  `id_profile` int(11) NOT NULL,
  `nm_profile` varchar(200) COLLATE utf8_bin NOT NULL,
  `nip` varchar(100) COLLATE utf8_bin NOT NULL,
  `jabatan` varchar(200) COLLATE utf8_bin NOT NULL,
  `pangkat` varchar(25) COLLATE utf8_bin NOT NULL,
  `foto` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data untuk tabel `profile`
--

INSERT INTO `profile` (`id_profile`, `nm_profile`, `nip`, `jabatan`, `pangkat`, `foto`) VALUES
(2, 'RAJA RINI ASTUTI, SE', '19740731 200803 2 001', 'Kasubbag Keuangan', 'eselon4', 'rajarini.jpeg'),
(3, 'YOSA TRISNA DWIPAYANA, SE. MH', '19790913 201001 1 008', 'Kasubbag Perencanaan & Evaluasi', 'eselon4', 'yosa.JPG'),
(4, 'WIWID SONDA, SE', '19781208 201101 2 002', 'Kasubbag Umum & Kepegawaian', 'eselon4', 'wiwid1.jpg'),
(5, 'ZUZIANI, SE', '19810902 200212 2 004', 'Kasubbid Anggaran Pendapatan Daerah', 'eselon4', 'zuziani.jpeg'),
(6, 'RANI RAFITRIYANI, S.STP', '19930115 201507 2 003', 'Kasubbid Anggaran Belanja Daerah', 'eselon4', 'rani.jpg'),
(7, 'YUSUP IHLAS, S.Kom', '19760910 200502 1 006', 'Kasubbid Pengendalian & Penyediaan Anggaran', 'eselon4', 'yusup.jpg'),
(8, 'YOSSE RIZAL, SE, MH', '19740821 199903 1 010', 'Kasubbid Pengeluaran Belanja Daerah', 'eselon4', 'yose.JPG'),
(9, 'M. FATIR ALFATAH, SE', '19820607 200701 1 011', 'Kasubbid Pengelolaan Kas Daerah', 'eselon4', 'fatir.jpg'),
(10, 'RAMADHONA, SIP', '19830626 200502 1 004', 'Kasubbid Belanja Pegawai', 'eselon4', 'rama.jpg'),
(11, 'SULVIA INDRIASARI, SE', '19730218 200604 2 002', 'Kasubbid Akuntansi Pendapatan', 'eselon4', 'sulvia.jpg'),
(12, 'TERRY S ISMAIL, SE', '19760620 200803 2 001', 'Kasubbid Akuntansi Belanja', 'eselon4', 'teri.jpg'),
(13, 'R. ZULKARNAIN NURZAMAN, SE', '19840911 200312 1 006', 'Kasubbid Pelaporan', 'eselon4', 'zulkarnain12.jpg'),
(14, 'AL HUSNI, S.Si, M.Pd', '19780819 200502 1 006', 'Kasubbid Perencanaan dan Analisa Kebutuhan Aset', 'eselon4', 'husni.jpg'),
(15, 'RINA ANGGREANI, ST, MT', '19740830 200502 2 006', 'Kasubbid Inventarisasi dan Pelaporan Aset Daerah', 'eselon4', 'rina.JPG'),
(16, 'SUHARTO, SE', '19670407 200502 2 006', 'Kasubbid Penilaian dan Pemanfaatan Aset', 'eselon4', 'suharto.JPG'),
(17, 'ZULFAHRI, SE', '19781119 200212 1 003', 'Kepala UPT BLUD', 'eselon4', 'zulfahri1.jpg'),
(18, 'BOBBY SYAFRIL LIZAN, SE', '19740426 200604 1 012', 'Kasubbag Tata Usaha', 'eselon4', 'boby.jpg'),
(23, 'WAN TAUFIK. ST.MT', '19780112 200312 1 006', 'Kabid Anggaran', 'eselon3', 'wantaufik.jpg'),
(24, 'PADLINSONO, SE, M.Si', '19761206 200212 1 012', 'Kabid Perbendaharaan', 'eselon3', 'padlinsono.jpg'),
(28, 'SANTI SUFRI, SE. Ak', '19760218 200212 2 005', 'Kabid Aset', 'eselon3', 'santi.jpg'),
(31, 'ISHAK, S.Kom, M. Si', '19840630 200604 1 007', 'Kabid Akuntansi dan Pelaporan', 'eselon3', 'default.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `reg_keuda`
--

CREATE TABLE `reg_keuda` (
  `id_reg` int(10) NOT NULL,
  `nm_req` varchar(50) COLLATE utf8_bin NOT NULL,
  `ket_reg` varchar(100) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Struktur dari tabel `renstra`
--

CREATE TABLE `renstra` (
  `id_renstra` int(11) NOT NULL,
  `jdl_renstra` varchar(100) COLLATE utf8_bin NOT NULL,
  `uraian` text COLLATE utf8_bin NOT NULL,
  `file` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data untuk tabel `renstra`
--

INSERT INTO `renstra` (`id_renstra`, `jdl_renstra`, `uraian`, `file`) VALUES
(1, 'RENCANA STRATEGIS\r\n2016-2021', 'Rencana Strategis yang disusun oleh Badan Pengelolaan Keuangan dan Aset Daerah Kota Batam merupakan langkah awal untuk melaksanakan tugas pokok dan fungsinya yang dalam pengelolaannya perlu melaksanakan pengelolaan keuangan dan aset daerah secara tertib, taat peraturan-perundangan, efektif, efisien, transparan, professional dan bertanggungjawab', 'Renstra BPKAD Kota Batam 2016-2021.pdf');

-- --------------------------------------------------------

--
-- Struktur dari tabel `strategi_bpkad`
--

CREATE TABLE `strategi_bpkad` (
  `id_strategi` int(2) NOT NULL,
  `isi_strategi` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `strategi_bpkad`
--

INSERT INTO `strategi_bpkad` (`id_strategi`, `isi_strategi`) VALUES
(1, '<li>Menerapkan tertibnya administrasi yang efisien, efektif dan dapat dipertanggungjwabkan.</li>'),
(2, '<li>Menerapkan sistem penyusunan, pengendalian dan penggunaan anggaran tepat waktu dan tepat sasaran.</li>'),
(3, '<li>Menerapkan pengelolaan penerimaan dan pengeluaran keuangan daerah sesuai dengan ketentuan yang berlaku.</li>'),
(4, '<li>Menerapkan penyelenggaraan sistem akuntansi dan pelaporan keuangan sesuai dengan ketentuan yang berlaku.</li>'),
(5, '<li>Meningkatkan validitas sistem pengelolaan aset daerah serta integrasi sistem pengelolaan keuangan dan aset daerah.</li>');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tabel_log`
--

CREATE TABLE `tabel_log` (
  `log_id` int(11) NOT NULL,
  `log_time` timestamp NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `log_user` varchar(255) DEFAULT NULL,
  `log_tipe` int(11) DEFAULT NULL,
  `log_desc` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tabel_log`
--

INSERT INTO `tabel_log` (`log_id`, `log_time`, `log_user`, `log_tipe`, `log_desc`) VALUES
(9, '2019-09-29 06:50:24', 'ardi', 2, 'menambahkan data'),
(10, '2019-09-29 07:11:52', 'ardi', 4, 'Hapus Data User'),
(11, '2019-09-29 12:25:03', 'ardi', 4, 'Hapus Data Foto Kegiatan'),
(12, '2019-10-01 03:44:55', 'ardi', 2, 'Tambah Data Regulasi Keuda'),
(13, '2019-10-01 03:45:48', 'ardi', 2, 'Tambah Data Regulasi Keuda'),
(14, '2019-10-01 03:48:57', 'ardi', 2, 'Tambah Data Regulasi Keuda'),
(15, '2019-10-01 03:49:20', 'ardi', 2, 'Tambah Data Regulasi Keuda'),
(16, '2019-10-01 03:49:51', 'ardi', 2, 'Tambah Data Regulasi Keuda'),
(17, '2019-10-01 03:51:03', 'ardi', 2, 'Tambah Data Regulasi Keuda'),
(18, '2019-10-01 04:22:58', 'ardi', 2, 'Tambah Data Regulasi Keuda'),
(19, '2019-10-01 04:23:12', 'ardi', 2, 'Tambah Data Regulasi Keuda'),
(20, '2019-10-01 04:25:43', 'ardi', 2, 'Tambah Data Regulasi Keuda'),
(21, '2019-10-01 04:26:06', 'ardi', 2, 'Tambah Data Regulasi Keuda'),
(22, '2019-10-01 04:26:27', 'ardi', 2, 'Tambah Data Regulasi Keuda'),
(23, '2019-10-01 04:27:29', 'ardi', 4, 'Hapus Data Regulasi Keuda'),
(24, '2019-10-01 04:28:08', 'ardi', 2, 'Tambah Data Regulasi Keuda'),
(25, '2019-10-01 04:28:27', 'ardi', 2, 'Tambah Data Regulasi Keuda'),
(26, '2019-10-01 04:28:41', 'ardi', 2, 'Tambah Data Regulasi Keuda'),
(27, '2019-10-01 04:28:55', 'ardi', 2, 'Tambah Data Regulasi Keuda'),
(28, '2019-10-02 04:22:39', 'ardi', 4, 'Hapus Data Foto Kegiatan'),
(29, '2019-10-14 01:39:03', 'ardi', 3, 'Edit Data Foto Kegiatan'),
(30, '2019-10-14 01:39:27', 'ardi', 3, 'Edit Data Foto Kegiatan'),
(31, '2019-10-14 01:41:09', 'ardi', 3, 'Edit Data Foto Kegiatan'),
(32, '2019-10-14 01:48:16', 'ardi', 3, 'Edit Data Foto Kegiatan'),
(33, '2019-10-14 01:48:29', 'ardi', 3, 'Edit Data Foto Kegiatan'),
(34, '2019-10-16 00:58:09', 'ardi', 2, 'Tambah Data Profile'),
(35, '2019-10-16 00:59:55', 'ardi', 2, 'Tambah Data User');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tb_barang`
--

CREATE TABLE `tb_barang` (
  `id_brg` int(11) NOT NULL,
  `nama_brg` varchar(120) COLLATE utf8_bin NOT NULL,
  `keterangan` varchar(225) COLLATE utf8_bin NOT NULL,
  `kategori` varchar(60) COLLATE utf8_bin NOT NULL,
  `harga` int(11) NOT NULL,
  `stok` int(4) NOT NULL,
  `foto` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data untuk tabel `tb_barang`
--

INSERT INTO `tb_barang` (`id_brg`, `nama_brg`, `keterangan`, `kategori`, `harga`, `stok`, `foto`) VALUES
(1, 'sepatu', 'sepatu pria', 'pakaian pria', 100, 2, 'asas'),
(1, 'sepatu', 'sepatu pria', 'pakaian pria', 100, 2, 'asas'),
(1, 'sepatu', 'sepatu pria', 'pakaian pria', 100, 2, 'asas');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_bpkad`
--

CREATE TABLE `user_bpkad` (
  `id_user` int(10) NOT NULL,
  `username` varchar(30) COLLATE utf8_bin NOT NULL,
  `pwd` varchar(100) COLLATE utf8_bin NOT NULL,
  `level` varchar(10) COLLATE utf8_bin NOT NULL,
  `bagian` varchar(50) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data untuk tabel `user_bpkad`
--

INSERT INTO `user_bpkad` (`id_user`, `username`, `pwd`, `level`, `bagian`) VALUES
(1, 'ardi', 'ardi', 'Operator', 'Anggaran'),
(3, 'ibnu', 'ibnu', 'Operator', 'Aset'),
(4, 'ardi', '202cb962ac59075b964b07152d234b70', '', ''),
(5, 'randi', 'ec1a08ca25857e260784856b3556804d', 'Admin', 'Programmer');

-- --------------------------------------------------------

--
-- Struktur dari tabel `visi_bpkad`
--

CREATE TABLE `visi_bpkad` (
  `id_visi` int(10) NOT NULL,
  `isi_visi` text COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data untuk tabel `visi_bpkad`
--

INSERT INTO `visi_bpkad` (`id_visi`, `isi_visi`) VALUES
(1, '<b>\"Mewujudkan Penyelenggaraan Pengelolaan Keuangan dan Aset Daerah Yang Tertib, Efektif, Efisien, Transparan dan Akuntabel\"</b>');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `anggaran`
--
ALTER TABLE `anggaran`
  ADD PRIMARY KEY (`id_anggaran`);

--
-- Indeks untuk tabel `apbd`
--
ALTER TABLE `apbd`
  ADD PRIMARY KEY (`id_apbd`);

--
-- Indeks untuk tabel `brt_bpkad`
--
ALTER TABLE `brt_bpkad`
  ADD PRIMARY KEY (`id_brt`),
  ADD UNIQUE KEY `id_ktg_brt` (`id_ktg_brt`),
  ADD UNIQUE KEY `id_user` (`id_user`);

--
-- Indeks untuk tabel `foto_kegiatan`
--
ALTER TABLE `foto_kegiatan`
  ADD PRIMARY KEY (`id_ft_kegiatan`);

--
-- Indeks untuk tabel `hibah`
--
ALTER TABLE `hibah`
  ADD PRIMARY KEY (`id_hibah`);

--
-- Indeks untuk tabel `kebijakan_bpkad`
--
ALTER TABLE `kebijakan_bpkad`
  ADD PRIMARY KEY (`id_kebijakan`);

--
-- Indeks untuk tabel `kegiatan_bpkad`
--
ALTER TABLE `kegiatan_bpkad`
  ADD PRIMARY KEY (`id_kegiatan_bpkad`),
  ADD UNIQUE KEY `id_ktg_kegiatan` (`id_ktg_kegiatan`),
  ADD UNIQUE KEY `id_user` (`id_user`);

--
-- Indeks untuk tabel `keuda`
--
ALTER TABLE `keuda`
  ADD PRIMARY KEY (`id_keuda`);

--
-- Indeks untuk tabel `ktg_brt`
--
ALTER TABLE `ktg_brt`
  ADD PRIMARY KEY (`id_ktg_brt`);

--
-- Indeks untuk tabel `ktg_kegiatan`
--
ALTER TABLE `ktg_kegiatan`
  ADD PRIMARY KEY (`id_ktg_kegiatan`);

--
-- Indeks untuk tabel `lkpd`
--
ALTER TABLE `lkpd`
  ADD PRIMARY KEY (`id_lkpd`);

--
-- Indeks untuk tabel `lkpj`
--
ALTER TABLE `lkpj`
  ADD PRIMARY KEY (`id_lkpj`);

--
-- Indeks untuk tabel `misi_bpkad`
--
ALTER TABLE `misi_bpkad`
  ADD PRIMARY KEY (`id_misi`);

--
-- Indeks untuk tabel `profile`
--
ALTER TABLE `profile`
  ADD PRIMARY KEY (`id_profile`);

--
-- Indeks untuk tabel `reg_keuda`
--
ALTER TABLE `reg_keuda`
  ADD PRIMARY KEY (`id_reg`);

--
-- Indeks untuk tabel `renstra`
--
ALTER TABLE `renstra`
  ADD PRIMARY KEY (`id_renstra`);

--
-- Indeks untuk tabel `strategi_bpkad`
--
ALTER TABLE `strategi_bpkad`
  ADD PRIMARY KEY (`id_strategi`);

--
-- Indeks untuk tabel `tabel_log`
--
ALTER TABLE `tabel_log`
  ADD PRIMARY KEY (`log_id`);

--
-- Indeks untuk tabel `user_bpkad`
--
ALTER TABLE `user_bpkad`
  ADD PRIMARY KEY (`id_user`);

--
-- Indeks untuk tabel `visi_bpkad`
--
ALTER TABLE `visi_bpkad`
  ADD PRIMARY KEY (`id_visi`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `anggaran`
--
ALTER TABLE `anggaran`
  MODIFY `id_anggaran` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT untuk tabel `apbd`
--
ALTER TABLE `apbd`
  MODIFY `id_apbd` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `brt_bpkad`
--
ALTER TABLE `brt_bpkad`
  MODIFY `id_brt` bigint(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `foto_kegiatan`
--
ALTER TABLE `foto_kegiatan`
  MODIFY `id_ft_kegiatan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT untuk tabel `hibah`
--
ALTER TABLE `hibah`
  MODIFY `id_hibah` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `kebijakan_bpkad`
--
ALTER TABLE `kebijakan_bpkad`
  MODIFY `id_kebijakan` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `kegiatan_bpkad`
--
ALTER TABLE `kegiatan_bpkad`
  MODIFY `id_kegiatan_bpkad` bigint(100) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `keuda`
--
ALTER TABLE `keuda`
  MODIFY `id_keuda` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT untuk tabel `ktg_brt`
--
ALTER TABLE `ktg_brt`
  MODIFY `id_ktg_brt` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `ktg_kegiatan`
--
ALTER TABLE `ktg_kegiatan`
  MODIFY `id_ktg_kegiatan` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `lkpd`
--
ALTER TABLE `lkpd`
  MODIFY `id_lkpd` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `lkpj`
--
ALTER TABLE `lkpj`
  MODIFY `id_lkpj` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `misi_bpkad`
--
ALTER TABLE `misi_bpkad`
  MODIFY `id_misi` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `profile`
--
ALTER TABLE `profile`
  MODIFY `id_profile` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT untuk tabel `reg_keuda`
--
ALTER TABLE `reg_keuda`
  MODIFY `id_reg` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `renstra`
--
ALTER TABLE `renstra`
  MODIFY `id_renstra` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `strategi_bpkad`
--
ALTER TABLE `strategi_bpkad`
  MODIFY `id_strategi` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `tabel_log`
--
ALTER TABLE `tabel_log`
  MODIFY `log_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT untuk tabel `user_bpkad`
--
ALTER TABLE `user_bpkad`
  MODIFY `id_user` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `visi_bpkad`
--
ALTER TABLE `visi_bpkad`
  MODIFY `id_visi` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
