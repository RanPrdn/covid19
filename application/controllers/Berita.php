<?php
class Berita extends CI_Controller{
	function __construct(){
		parent::__construct();
		     //load libary pagination
        $this->load->library('pagination');


		$this->load->model('m_berita');
	}
	function index(){
		$this->load->view('frontend/_partialsmycss/head.php');
        $this->load->view('frontend/_partialsgaleri/navbar.php');
		

		//konfigurasi pagination
        $config['base_url'] = site_url('berita/index'); //site url
        $config['total_rows'] = $this->db->count_all('foto_kegiatan'); //total row
        $config['per_page'] =4;  //show record per halaman
        $config["uri_segment"] = 3;  // uri parameter
        $choice = $config["total_rows"] / $config["per_page"];
        $config["num_links"] = floor($choice);

        // Membuat Style pagination untuk BootStrap v4
        $config['first_link']       = 'First';
        $config['last_link']        = 'Last';
        $config['next_link']        = 'Next';
        $config['prev_link']        = 'Prev';
        $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
        $config['full_tag_close']   = '</ul></nav></div>';
        $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close']    = '</span></li>';
        $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close']  = '</span>Next</li>';
        $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close']  = '</span></li>';

        $this->pagination->initialize($config);
        $data['page'] = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;

        //panggil function get_berita_list yang ada pada model berita_model. 
        $data['data'] = $this->m_berita->get_berita_list($config["per_page"], $data['page']);           

        $data['pagination'] = $this->pagination->create_links();

        //load view berita view
        $this->load->view('frontend/_partialsgaleri/v_post_list',$data);


        // $x['data']=$this->m_berita->get_all_berita();
		// $this->load->view('frontend/_partialsgaleri/v_post_list',$x);
		$this->load->view('frontend/_partialsmycss/alamat.php');
        $this->load->view('frontend/_partialsmycss/footer.php');
        $this->load->view('frontend/_partialsmyjs/js.php'); 
	}

	function view(){
		$kode=$this->uri->segment(3);
		$x['data']=$this->m_berita->get_berita_by_kode($kode);
		$this->load->view('frontend/_partialsgaleri/v_post_view',$x);
	}

}