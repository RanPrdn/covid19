<?php
	class Data_penyalur extends CI_Controller
	{
		public function index()
		{
			$data['user'] = $this->model_penyalur->tampil_data()->result();
			
			$this->load->view('templates_admin/header');
			$this->load->view('templates_admin/navbar');
			$this->load->view('templates_admin/sidebar');
			$this->load->view('admin/data_penyalur', $data);
			$this->load->view('templates_admin/content');
			$this->load->view('templates_admin/footer');
		}

		public function tambah_aksi()
		{
			$tgl_penyaluran 	   = $this->input->post('tgl_penyaluran');
			$skpd      			   = $this->input->post('skpd');
			$penerima      		   = $this->input->post('penerima');
			$nama_pemberi   	   = $this->input->post('nama_pemberi');
			$nama_barang      	   = $this->input->post('nama_barang');
			$jumlah   	  		   = $this->input->post('jumlah');
			$satuan_salur  		   = $this->input->post('satuan_salur');
			$ket     			   = $this->input->post('ket');

			$data = array (
				'tgl_penyaluran'	 => $tgl_penyaluran,
				'skpd' 			     => $skpd,
				'penerima' 			 => $penerima,
				'nama_pemberi' 		 => $nama_pemberi,
				'nama_barang'		 => $nama_barang,
				'jumlah'			 => $jumlah,
				'satuan_salur' 	 	 => $satuan_salur,
				'ket' 				 => $ket
			);

		$this->model_penyalur->tambah_salur($data, 'penyaluran');
		redirect('admin/data_penyalur');
		}

		public function edit($id)
		{
			$where = array('id_terima' =>$id);
			$data['user'] = $this->model_pemberi->edit_terima($where, 'terima')->result();
			$this->load->view('templates_admin/header');
			$this->load->view('templates_admin/navbar');
			$this->load->view('templates_admin/sidebar');
			$this->load->view('admin/edit_pemberi', $data);
			$this->load->view('templates_admin/content');
			$this->load->view('templates_admin/footer');
		}	

		public function update()
		{
			$id       = $this->input->post('id_terima');
			
			$tgl_terima 	   = $this->input->post('tgl_terima');
			$nama_pemberi      = $this->input->post('nama_pemberi');
			$nama_barang       = $this->input->post('nama_barang');
			$jenis_barang      = $this->input->post('jenis_barang');
			$volume_beri       = $this->input->post('volume_beri');
			$satuan_beri   	   = $this->input->post('satuan_beri');
			$hrga_satuan  	   = $this->input->post('hrga_satuan');
			$nilai_barang      = $this->input->post('nilai_barang');

			$data = array (
				'tgl_terima'	 => $tgl_terima,
				'nama_pemberi' 	 => $nama_pemberi,
				'nama_barang' 	 => $nama_barang,
				'jenis_barang'	 => $jenis_barang,
				'volume_beri'	 => $volume_beri,
				'satuan_beri' 	 => $satuan_beri,
				'hrga_satuan' 	 => $hrga_satuan,
				'nilai_barang' 	 => $nilai_barang
			);

			$where = array (
				'id_terima' =>$id
			);
			$this->model_pemberi->update_data($where, $data, 'terima');
			redirect('admin/data_pemberi');
		}

		public function hapus($id)
		{
			$where = array('id_penyaluran' => $id);
			$this->model_penyalur->hapus_data($where, 'penyaluran');
			redirect('admin/data_penyalur');
		}
	}
?>