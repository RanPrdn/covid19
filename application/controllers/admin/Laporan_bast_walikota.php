<?php
	class Laporan_bast_walikota extends CI_Controller{

            function __construct() {
                  parent::__construct();
                  $this->load->library('pdf');
            }
    
            function index(){
                $tgl_terima 	   = $this->input->post('tgl_terima'); 
                $nama_pemberi 	   = $this->input->post('nama_pemberi'); 

                $where1 = array (
					'tgl_terima'   =>$tgl_terima,
				);

				$where2 = array (
					'nama_pemberi'   =>$nama_pemberi,
				);

				$data['user'] = $this->model_laporan_bast->tampil_data_walikota($where1, $where2)->result();

                $this->load->view('admin/laporan_bast_walikota', $data);
                  
            }
	}
?>