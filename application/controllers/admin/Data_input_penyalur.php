<?php
	class Data_input_penyalur extends CI_Controller
	{
		public function index()
		{
			//$data['user'] = $this->model_pemberi->tampil_data()->result();
			$data['record']=  $this->model_input_penyalur->tampil_data();

			$this->load->view('templates_admin/header');
			$this->load->view('templates_admin/navbar');
			$this->load->view('templates_admin/sidebar');
			$this->load->view('admin/data_input_penyalur', $data);
			$this->load->view('templates_admin/content');
			$this->load->view('templates_admin/footer');
		}

		public function cari(){
	        $nama_pemberi=$_GET['nama_pemberi'];
	        $cari =$this->model_input_penyalur->cari($nama_pemberi)->result();
	        echo json_encode($cari);
    	} 
    	
    	public function tambah_aksi()
		{
			$tgl_penyaluran 	   = $this->input->post('tgl_penyaluran');
			$skpd      			   = $this->input->post('skpd');
			$penerima      		   = $this->input->post('penerima');
			$id_terima   	   	   = $this->input->post('id_terima');
			$nama_barang      	   = $this->input->post('nama_barang');
			$jumlah   	  		   = $this->input->post('jumlah');
			$satuan_salur  		   = $this->input->post('satuan_salur');
			$ket     			   = $this->input->post('ket');

			$data = array (
				'id_terima' 		 => $id_terima,
				'tgl_penyaluran'	 => $tgl_penyaluran,
				'skpd' 			     => $skpd,
				'penerima' 			 => $penerima,
				'nama_barang'		 => $nama_barang,
				'jumlah'			 => $jumlah,
				'satuan_salur' 	 	 => $satuan_salur,
				'ket' 				 => $ket
			);

		$this->model_penyalur->tambah_salur($data, 'penyaluran');
		redirect('admin/data_penyalur');
		}

	}
?>