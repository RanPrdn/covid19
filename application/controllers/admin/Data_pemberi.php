<?php
	class Data_pemberi extends CI_Controller
	{
		public function index()
		{
			$data['user'] = $this->model_pemberi->tampil_data()->result();
			$this->load->view('templates_admin/header');
			$this->load->view('templates_admin/navbar');
			$this->load->view('templates_admin/sidebar');
			$this->load->view('admin/data_pemberi', $data);
			$this->load->view('templates_admin/content');
			$this->load->view('templates_admin/footer');
		}

		public function tambah_aksi()
		{
			$tgl_terima 	   = $this->input->post('tgl_terima');
			$nama_pemberi      = $this->input->post('nama_pemberi');
			$jabatan	       = $this->input->post('jabatan');
			$alamat		       = $this->input->post('alamat');
			$nama_barang       = $this->input->post('nama_barang');
			$jenis_barang      = $this->input->post('jenis_barang');
			$volume_beri       = $this->input->post('volume_beri');
			$satuan_beri   	   = $this->input->post('satuan_beri');
			$hrga_satuan  	   = $this->input->post('hrga_satuan');
			$nilai_barang      = $this->input->post('nilai_barang');

			$data = array (
				'tgl_terima'	 => $tgl_terima,
				'nama_pemberi' 	 => $nama_pemberi,
				'jabatan'	 	 => $jabatan,
				'alamat'	 	 => $alamat,
				'nama_barang' 	 => $nama_barang,
				'jenis_barang'	 => $jenis_barang,
				'volume_beri'	 => $volume_beri,
				'satuan_beri' 	 => $satuan_beri,
				'hrga_satuan' 	 => $hrga_satuan,
				'nilai_barang' 	 => $nilai_barang,
			);

		$this->model_pemberi->tambah_beri($data, 'terima');
		redirect('admin/data_pemberi');
		}

		public function edit($id)
		{
			$where = array('id_terima' =>$id);
			$data['user'] = $this->model_pemberi->edit_terima($where, 'terima')->result();
			$this->load->view('templates_admin/header');
			$this->load->view('templates_admin/navbar');
			$this->load->view('templates_admin/sidebar');
			$this->load->view('admin/edit_pemberi', $data);
			$this->load->view('templates_admin/content');
			$this->load->view('templates_admin/footer');
		}	

		public function update()
		{
			$id       = $this->input->post('id_terima');
			
			$tgl_terima 	   = $this->input->post('tgl_terima');
			$nama_pemberi      = $this->input->post('nama_pemberi');
			$nama_barang       = $this->input->post('nama_barang');
			$jenis_barang      = $this->input->post('jenis_barang');
			$volume_beri       = $this->input->post('volume_beri');
			$satuan_beri   	   = $this->input->post('satuan_beri');
			$hrga_satuan  	   = $this->input->post('hrga_satuan');
			$nilai_barang      = $this->input->post('nilai_barang');

			$data = array (
				'tgl_terima'	 => $tgl_terima,
				'nama_pemberi' 	 => $nama_pemberi,
				'nama_barang' 	 => $nama_barang,
				'jenis_barang'	 => $jenis_barang,
				'volume_beri'	 => $volume_beri,
				'satuan_beri' 	 => $satuan_beri,
				'hrga_satuan' 	 => $hrga_satuan,
				'nilai_barang' 	 => $nilai_barang
			);

			$where = array (
				'id_terima' =>$id
			);
			$this->model_pemberi->update_data($where, $data, 'terima');
			redirect('admin/data_pemberi');
		}

		public function hapus($id)
		{
			$where = array('id_terima' => $id);
			$this->model_user->hapus_data($where, 'terima');
			redirect('admin/data_pemberi');
		}
	}
?>