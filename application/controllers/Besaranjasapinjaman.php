<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Besaranjasapinjaman extends CI_Controller 
    {
        public function index()
        { 
            $this->load->view('frontend/_partialsmycss/head.php');
            $this->load->view('frontend/dana_bergulir/besaran_jasa_pinjaman/navbar.php');
            $this->load->view('frontend/dana_bergulir/besaran_jasa_pinjaman/content');
            $this->load->view('frontend/_partialsmycss/alamat.php');
            $this->load->view('frontend/_partialsmycss/footer.php');
            $this->load->view('frontend/_partialsmyjs/js.php'); 
        }
    }
?>
