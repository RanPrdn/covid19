<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class View extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('View_model');
    }
   
    public function get_view_result()
    {

            $ViewData = $this->input->post('ViewData');

            if(isset($ViewData) and !empty($ViewData)){
                $records = $this->View_model->get_view_data($ViewData);
                $output = '';
                foreach($records->result_array() as $row){
                    $output .= '
                         
                            <h4 class="text-center"><b><u>'.$row["nm_profile"].'</b></u></h4><br>
                            <center><img style="width:50%;" src="'.base_url().'assets/images/profil/'.$row["foto"].'"></center>'
                            ;


                }               
                echo $output;
            } 
    }
}
