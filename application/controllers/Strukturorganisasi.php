<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Strukturorganisasi extends CI_Controller 
    {
        public function index()
        {            
            $this->load->view('frontend/_partialsmycss/head.php');
            $this->load->view('frontend/_partialsstrukturorganisasi/navbar.php');
            $this->load->view('frontend/_partialsstrukturorganisasi/content');
            $this->load->view('frontend/_partialsmycss/alamat.php');
            $this->load->view('frontend/_partialsmycss/footer.php');
            $this->load->view('frontend/_partialsmyjs/js.php'); 
        }
    }
?>
