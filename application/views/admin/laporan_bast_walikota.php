<?php

// create new PDF document
$pdf = new TCPDF();


// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf->SetFont('times', 'BC', 12);

// add a page
$pdf->AddPage();
// set some text to print
$SetTitle = <<<EOD

BERITA ACARA SERAH TERIMA
DARI
KONSULAT JENDERAL SINGAPURA
KEPADA
PEMERINTAH KOTA BATAM

EOD;

// print a block of text using Write()
$pdf->Write(0, $SetTitle, '', 0, 'C', true, 0, false, false, 0);



// set font
$pdf->SetFont('times', 'BU', 12);
// set some text to print

$biodata = <<<EOD
<br><hr><br>

EOD;

// print a block of text using Write()
$pdf->WriteHTMLCell(0,0,'','',$biodata,0,1,0,true,'L',true);

foreach ($data as $key => $b) {

// set font
$pdf->SetFont('times', '', 11);


$isibiodata = <<<EOD
<p>Pada hari ini Senin tanggal enam bulan April Tahun dua ribu dua puluh (06-04- 2020) kami yang bertanda tangan di bawah ini: </p>

<p>
1.  &nbsp;&nbsp;  Nama   &nbsp; :  <b>$b->nama_pemberi</b> <br>
    &nbsp;&nbsp;&nbsp;&nbsp; Jabatan   :  <b>KETUA ASOSIASI DISTRIBUTOR BAPOK KOTA BATAM</b> <br>
    &nbsp;&nbsp;&nbsp;&nbsp; Alamat    :  <b>KOTA BATAM</b> <br>
   &nbsp;&nbsp;&nbsp;&nbsp; Dalam hal ini bertindak untuk dan atas nama Asosiasi Distributor Bapok Kota Batam selanjutnya disebut PIHAK PERTAMA;
</p>
<p>
2.  &nbsp;&nbsp; Nama &nbsp; :   <b>H. JEFRIDIN, M.Pd</b><br>
    &nbsp;&nbsp;&nbsp;&nbsp; Jabatan :  <b>SEKRETARIS DAERAH KOTA BATAM</b><br>
   &nbsp;&nbsp;&nbsp;&nbsp; Alamat  : Jl. Engku Putri No. 1 Batam Centre<br>
 &nbsp;&nbsp;&nbsp;&nbsp; Dalam hal ini bertindak untuk dan atas nama <b>Pemerintah Kota Batam</b> berdasarkan Keputusan Walikota Batam Nomor 77/BKD/HK/XII/2016 tanggal 27 Desember 2016  selanjutnya <b>PIHAK KEDUA</b>;
</p>
<p>
Kedua belah pihak telah sepakat untuk membuat Berita Acara Serah Terima dari <b>PIHAK PERTAMA</b> kepada <b>PIHAK KEDUA</b> berupa :
Jenis Barang  : Paket Sembako 
Jumlah    : 1.588 Paket
Nilai   : Rp. 206.440.000
</p>

<p>
Yang dipergunakan dalam rangka penanganan wabah COVID -19 di Kota Batam, sesuai dengan lampiran yang tidak dapat dipisahkan dari Berita Acara Serah Terima ini. 
</p>

<p>
Dengan ditandatangainya berita acara serah terima ini, maka <b>PIHAK PERTAMA</b> dan <b>PIHAK KEDUA</b> menyetujui bahwa barang yang diserahkankan menjadi tanggung jawab <b>PIHAK KEDUA</b>.
</p>

<p>
Demikian berita acara serah terima ini dibuat dan ditandatangani dengan sebenarnya untuk dipergunakan sebagaimana mestinya.


</p>

EOD;
}
// print a block of text using Write()
$pdf->WriteHTMLCell(0,0,'','',$isibiodata,0,1,0,true,'L',true);

// set font
$pdf->SetFont('times', 'BU', 12);
// set some text to print

// set font
$pdf->SetFont('helvetica', '', 8);

// --- Scaling ---------------------------------------------

// Start Transformation
$pdf->StartTransform();
$pdf->SetFont('times', 'B', 9);
// Scale by 150% centered by (50,80) which is the lower left corner of the rectangle
$pdf->ScaleXY(111, 455,-1500);
//$pdf->Rect(60, 65, 20, 30, 'D');
$pdf->Text(68, 70 , 'PIHAK KEDUA');
$pdf->Text(66, 75, '');
$pdf->Text(66, 90, 'H. JEFRIDIN,M.Pd');
// Stop Transformation
$pdf->StopTransform();

$pdf->ScaleXY(111, 63,-1500);
$pdf->SetFont('times', 'B', 9);
$pdf->Text(135, 70 , 'PIHAK PERTAMA');
$pdf->SetFont('times', 'B', 9);
$pdf->Text(140, 90 , 'ARYANTO');
// $pdf->SetFont('times', '', 12);
// foreach ($data->result() as $row) {
//  $table = $row->nama_lengkap.'<br>'
//      .$row->no_ktp;
      
        
// }

// $pdf->WriteHTMLCell(0,0,'','',$table,0,1,0,true,'C',true);

// ---------------------------------------------------------

//Close and output PDF document
ob_clean();
$pdf->Output('laporan_bast_walikota.pdf','I');

//============================================================+
// END OF FILE
//============================================================+