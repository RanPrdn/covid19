<div class="main-content">
  <div class="main-content-inner">
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
      <ul class="breadcrumb">
        <li>
          <i class="ace-icon fa fa-home home-icon"></i>
          <a href="#">Home</a>
        </li>
        <li class="active">Log Aktivitas</li>
      </ul><!-- /.breadcrumb -->

      <div class="nav-search" id="nav-search">
        <form class="form-search">
          <span class="input-icon">
            <input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
            <i class="ace-icon fa fa-search nav-search-icon"></i>
          </span>
        </form>
      </div><!-- /.nav-search -->
    </div>
<!--content admin/content-->
    <div class="page-content">
      <div class="page-header"> 
          <small>
            <i class="ace-icon fa fa-angle-double-right"></i>
            Data Log administrator bpkad-batam
          </small>
        </h1>
      </div><!-- /.page-header -->

      <div class="row">
        <div class="col-xs-12">
                <!-- PAGE CONTENT BEGINS -->
<!--- isi content admin/data_user -->
          <div class="content-wrapper">
            <div class="container-fluid">   
              <div class="row">
<!--PAGE NEXT--><div class="row">
                  <div class="col-xs-12">
                    <form>
                      <div class="modal-content">
                    <div class="modal-header">
                      <center><h5 class="modal-title" id="exampleModalLabel">FORM INPUT PENYERAHAN BANTUAN</h5></center>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                      <form enctype="multipart/form-data">

                        <div class="form-group">
                          <div class="form-group col-md-6">
                            <label for="inputEmail4">Tanggal</label>
                            <input type="date" class="form-control" id="inputEmail4" name="tgl_terima">
                          </div>
                          <div class="form-group col-md-6">
                            <label for="inputPassword4">Pemberi Bantuan</label>
                            <input list="data_pemberi" type="text" class="form-control" id="inputPassword4" name="nama_pemberi">
                          </div>
                        </div>

                        <div class="form-group">
                          <center>
                            <label >Ditanda Tangani Oleh</label>
                            <div class="form-row">
                              <a href="<?php echo base_url('admin/laporan_bast_walikota') ?>" type="button" class="btn btn-success">WALIKOTA</a>
                              <button type="button" class="btn btn-primary">SEKDA</button>
                            </div>
                          </center>
                        </div>

                      </form>
                  </div> 
                </div>
              </div>
            </div>                  
          </div>  
        </div>
      </div>
    </div>             
  </div>
</div>

<datalist id="data_pemberi">
    <?php
    foreach ($record->result() as $b)
    {
        echo "<option value='$b->nama_pemberi'></option>";
    }
    ?>
    
</datalist>   