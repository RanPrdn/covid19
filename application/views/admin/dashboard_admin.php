<div class="main-content">
  <div class="main-content-inner">
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
      <ul class="breadcrumb">
        <li>
          <i class="ace-icon fa fa-home home-icon"></i>
          <a href="#">Home</a>
        </li>
        <li class="active">Log Aktivitas</li>
      </ul><!-- /.breadcrumb -->

      <div class="nav-search" id="nav-search">
        <form class="form-search">
          <span class="input-icon">
            <input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
            <i class="ace-icon fa fa-search nav-search-icon"></i>
          </span>
        </form>
      </div><!-- /.nav-search -->
    </div>
<!--content admin/content-->
    <div class="page-content">
      <div class="page-header"> 
          <small>
            <i class="ace-icon fa fa-angle-double-right"></i>
            Data Log administrator bpkad-batam
          </small>
        </h1>
      </div><!-- /.page-header -->

      <div class="row">
        <div class="col-xs-12">
                <!-- PAGE CONTENT BEGINS -->
<!--- isi content admin/data_user -->
          <div class="content-wrapper">
            <div class="container-fluid">   
              <div class="row">
<!--PAGE NEXT--><div class="row">
                  <div class="col-xs-12">
                    <div>
                      <table id="table-user" class="table table-striped table-bordered" width="100%" cellspacing="0">
                        <thead>
                          <tr>
                            <th class="center">No</th>
                            <th class="center">Tanggal Terima</th>
                            <th class="center">Pemberi Bantuan</th>
                            <th class="center">Nama Barang</th>
                            <th class="center">Jenis Barang</th>
                            <th class="center">Volume</th>
                            <th class="center">Satuan</th>
                            <th class="center">Harga Satuan</th>
                            <th class="center">Nilai Barang</th>
                            <th class="center">Tanggal Penyaluran</th>
                            <th class="center">Disalurkan Ke SPKD</th>
                            <th class="center">Penerima</th>
                            <th class="center">Jumlah</th>
                            <th class="center">Satuan</th>
                            <th class="center">Sisa Barang</th>
                            <th class="center">Keterangan</th>
                          </tr>
                        </thead>

                        <tbody>
                      
                          <?php foreach ($user as $key => $us):

                            $no=1;

                            $volume_beri1 = $us->volume_beri;
                            $jumlah1      = $us->jumlah;
                            $sisa = $volume_beri1-$jumlah1;
                          ?>
                          </tr>
                            <td><?php  echo $no+$key ?> </td>
                            <td><?php  echo $us->tgl_terima ?> </td>
                            <td><?php  echo $us->nama_pemberi ?> </td>
                            <td><?php  echo $us->nama_barang ?> </td>
                            <td><?php  echo $us->jenis_barang ?> </td>
                            <td><?php  echo $us->volume_beri ?> </td>
                            <td><?php  echo $us->satuan_beri ?> </td>
                            <td><?php  echo $us->hrga_satuan ?> </td>
                            <td><?php  echo $us->nilai_barang ?> </td>
                            <td><?php  echo $us->tgl_penyaluran ?> </td>
                            <td><?php  echo $us->skpd ?> </td>
                            <td><?php  echo $us->penerima ?> </td>
                            <td><?php  echo $us->jumlah ?> </td>
                            <td><?php  echo $us->satuan_salur ?> </td>
                            <td><?php  echo $sisa ?> </td>
                            <td><?php  echo $us->ket ?> </td>
                          </tr>
                            <?php endforeach ?>
                        </tbody>
                      </table>
                    </div>
                  </div> 
                </div>
              </div>
            </div>                  
          </div>  
        </div>
      </div>
    </div>             
  </div>
</div>