<div class="main-content">
  <div class="main-content-inner">
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
      <ul class="breadcrumb">
        <li>
          <i class="ace-icon fa fa-home home-icon"></i>
          <a href="#">Home</a>
        </li>
        <li class="active">Edit Data user</li>
      </ul><!-- /.breadcrumb -->

      <div class="nav-search" id="nav-search">
        <form class="form-search">
          <span class="input-icon">
            <input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
            <i class="ace-icon fa fa-search nav-search-icon"></i>
          </span>
        </form>
      </div><!-- /.nav-search -->
    </div>
<!--content admin/content-->
    <div class="page-content">
      <div class="page-header"> 
          <small>
            <i class="ace-icon fa fa-angle-double-right"></i>
            Data user administrator bpkad-batam
          </small>
        </h1>
      </div><!-- /.page-header -->

  <div class="content-wrapper">
    <div class="container-fluid">
      <center><h3>FORM INPUT PENYERAHAN BANTUAN</h3></center>
    
     
        <form method="post"  action="<?php echo base_url(). 'admin/data_input_penyalur/tambah_aksi' ?>">
          
          <div class="form-group">
                          <label>Tanggal Penyaluran</label>
                          <input type="date" name="tgl_penyaluran" class="form-control" required="">
                        </div>

                        <div class="form-group">
                          <label>Disalurkan Ke SKPD</label>
                          <input type="text" name="skpd" class="form-control" required="">
                        </div>

                        <div class="form-group">
                          <label>Penerima</label>
                          <input type="text" name="penerima" class="form-control" required="">
                        </div>

                        <div class="form-group">
                          <label>Pemberi Bantuan</label>
                          <input list="data_pemberi" type="text" id="nama_pemberi" name="nama_pemberi" class="form-control" onchange="return autofill();">
                          <input type="hidden" id="id_terima" name="id_terima">
                        </div>

                        <div class="form-group">
                          <label>Nama Barang</label>
                          <input type="text" name="nama_barang" id="nama_barang" class="form-control" readonly="">
                        </div>

                        <div class="form-group">
                          <label>Volume Barang Yang Diterima</label>
                          <input type="text" name="nama_barang" id="volume_beri" class="form-control" readonly="">
                        </div>

                        <div class="form-group">
                          <label>Volume</label>
                          <input type="text" name="jumlah" class="form-control" required="">
                        </div>

                        <div class="form-group">
                          <label>Satuan</label>
                          <input type="text" name="satuan_salur" id="satuan_beri" class="form-control" readonly="">
                        </div>

                        <div class="form-group">
                          <label>Keterangan</label>
                          <input type="text" name="ket" class="form-control">
                        </div>
          <button type="submit" class="btn btn-primary btn-sm mt-2"> Simpan</button>
        </form>

    </div>
  </div>
  </div>
</div>

<datalist id="data_pemberi">
    <?php
    foreach ($record->result() as $b)
    {
        echo "<option value='$b->nama_pemberi'>$b->nama_barang</option>";
    }
    ?>
    
</datalist>   

<script>
    function autofill(){
        var nama_pemberi =document.getElementById('nama_pemberi').value;
        $.ajax({
                       url:"<?php echo base_url('admin/data_input_penyalur/cari');?>",
                       data:'&nama_pemberi='+nama_pemberi,
                       success:function(data){
                           var hasil = JSON.parse(data);  
          
      $.each(hasil, function(key,val){ 
        
        document.getElementById('id_terima').value=val.id_terima;
        document.getElementById('nama_pemberi').value=val.nama_pemberi;
        document.getElementById('nama_barang').value=val.nama_barang;
        document.getElementById('volume_beri').value=val.volume_beri;
        document.getElementById('satuan_beri').value=val.satuan_beri;
        });
      }
                   });
                  
    }
</script>