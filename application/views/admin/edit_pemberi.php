<div class="main-content">
  <div class="main-content-inner">
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
      <ul class="breadcrumb">
        <li>
          <i class="ace-icon fa fa-home home-icon"></i>
          <a href="#">Home</a>
        </li>
        <li class="active">Edit Data user</li>
      </ul><!-- /.breadcrumb -->

      <div class="nav-search" id="nav-search">
        <form class="form-search">
          <span class="input-icon">
            <input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
            <i class="ace-icon fa fa-search nav-search-icon"></i>
          </span>
        </form>
      </div><!-- /.nav-search -->
    </div>
<!--content admin/content-->
    <div class="page-content">
      <div class="page-header"> 
          <small>
            <i class="ace-icon fa fa-angle-double-right"></i>
            Data user administrator bpkad-batam
          </small>
        </h1>
      </div><!-- /.page-header -->

	<div class="content-wrapper">
		<div class="container-fluid">
			<center><h3>EDIT DATA USER</h3></center>
	 	
			<?php foreach ($user as $key => $us) : ?>
				<form method="post"  action="<?php echo base_url(). 'admin/data_pemberi/update' ?>">
					<div class="form-group">
						<label>Tanggal Terima</label>
						<input type="hidden" name="id_terima" class="form-control" value="<?php echo $us->id_terima?>">
						<input type="text" name="tgl_terima" class="form-control" value="<?php echo $us->tgl_terima?>">
					</div>

					<div class="form-group">
						<label>Nama Pemberi</label>
						<input type="text" name="nama_pemberi" class="form-control" value="<?php echo $us->nama_pemberi?>">
					</div>

					<div class="form-group">
						<label>Nama Barang</label>
						<input type="text" name="nama_barang" class="form-control" value="<?php echo $us->nama_barang?>">
					</div>

					<div class="form-group">
						<label>Jenis Barang</label>
						<?php 
							$jenis_barangdatabase = $us->jenis_barang;
						?>
							<select type="text" name="jenis_barang" class="form-control" value="<?php echo $us->jenis_barang?>">
					              <option <?php if( $jenis_barangdatabase=='MEDIS'){echo "selected"; } ?> value='MEDIS'>MEDIS</option>
					              <option <?php if( $jenis_barangdatabase=='NON MEDIS'){echo "selected"; } ?> value='NON MEDIS'>NON MEDIS</option>
					              <option <?php if( $jenis_barangdatabase=='BAHAN MAKANAN'){echo "selected"; } ?> value='BAHAN MAKANAN'>BAHAN MAKANAN</option>
					             
		            		</select>
					</div>

					<div class="form-group">
						<label>Volume</label>
						<input type="text" name="volume_beri" class="form-control" value="<?php echo $us->volume_beri?>">
					</div>

					<div class="form-group">
						<label>Satuan</label>
						<input type="text" name="satuan_beri" class="form-control" value="<?php echo $us->satuan_beri?>">
					</div>

					<div class="form-group">
						<label>Harga Satuan</label>
						<input type="text" name="hrga_satuan" class="form-control" value="<?php echo $us->hrga_satuan?>">
					</div>

					<div class="form-group">
						<label>Nilai Barang</label>
						<input type="text" name="nilai_barang" class="form-control" value="<?php echo $us->nilai_barang?>">
					</div>

					<button type="submit" class="btn btn-primary btn-sm mt-2"> Simpan</button>
				</form>

			<?php endforeach;?>
		</div>
	</div>
	</div>
</div>