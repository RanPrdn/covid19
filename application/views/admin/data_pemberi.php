<div class="main-content">
  <div class="main-content-inner">
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
      <ul class="breadcrumb">
        <li>
          <i class="ace-icon fa fa-home home-icon"></i>
          <a href="#">Home</a>
        </li>
        <li class="active">User</li>
      </ul><!-- /.breadcrumb -->
    </div>
<!--content admin/content-->
    <div class="page-content">
      <div class="page-header"> 
          <small>
            <i class="ace-icon fa fa-angle-double-right"></i>
            Data user administrator bpkad-batam
          </small>
        </h1>
      </div>
      <!-- /.page-header -->
      <div class="row">
        <div class="col-xs-12">
          <div class="pull-left tableTools-container">
            <div class="dt-buttons btn-overlap btn-group">
                <button class="btn btn-lg btn-success" data-toggle="modal" data-target="#tambahberi">
                          <i class="ace-icon fa fa-plus"></i>
                          Pemberi Bantuan
                </button>
            </div>
          </div>

          <div class="content-wrapper">
            <div class="container-fluid">   
              <div class="row">
<!--PAGE NEXT-->  <div class="row">
                    <div class="col-xs-12">
                    <!-- div.table-responsive -->
                    <!-- div.dataTables_borderWrap -->
                      <div>
                        <table id="table-user" class="table table-striped table-bordered" width="100%" cellspacing="0">
                          <thead>
                            <tr>
                              <th class="center">No</th>
                              <th class="center">Tanggal Terima</th>
                              <th class="center">Nama Pemberi</th>
                              <th Class="Center">Nama Barang</th>
                              <th Class="center">Jenis Barang</th>
                              <th Class="center">Volume</th>
                              <th Class="center">Satuan</th>
                              <th Class="center">Harga Satuan</th>
                              <th Class="center">Nilai Barang</th>
                              <th class="Center">Aksi</th>
                            </tr>
                          </thead>

                          <tbody>
                        
                            <?php foreach ($user as $key => $us):

                              $no=1;
                            ?>
                             </tr>
                                <td><?php  echo $no+$key ?> </td>
                                <td><?php  echo $us->tgl_terima ?></td>
                                <td><?php  echo $us->nama_pemberi ?></td>
                                <td><?php  echo $us->nama_barang ?></td>
                                <td><?php  echo $us->jenis_barang ?></td>
                                <td><?php  echo $us->volume_beri ?></td>
                                <td><?php  echo $us->satuan_beri ?></td>
                                <td><?php  echo $us->hrga_satuan ?></td>
                                <td><?php  echo $us->nilai_barang ?></td>
                               <td>
                                  <div class="hidden-sm hidden-xs action-buttons">
                                    <a class="green" href="<?php echo base_url('admin/data_pemberi/edit/'.$us->id_terima)?>">
                                      <i class="ace-icon fa fa-pencil bigger-130"></i>
                                    </a>

                                    <a onclick="javascript: return confirm('Anda Yakin Hapus')" class="red" href="<?php echo base_url('admin/data_pemberi/hapus/'.$us->id_terima)?>">
                                      <i class="ace-icon fa fa-trash-o bigger-130"></i>
                                    </a>
                                  </div>

                                  <div class="hidden-md hidden-lg">
                                    <div class="inline pos-rel">
                                      <button class="btn btn-minier btn-yellow dropdown-toggle" data-toggle="dropdown" data-position="auto">
                                        <i class="ace-icon fa fa-caret-down icon-only bigger-120"></i>
                                      </button>
                                      <ul class="dropdown-menu dropdown-only-icon dropdown-yellow dropdown-menu-right dropdown-caret dropdown-close">
                                        <li>
                                          <a href="<?php echo base_url('admin/data_pemberi/edit/'.$us->id_terima)?>" class="tooltip-success" data-rel="tooltip" title="Edit">
                                            <span class="green">
                                              <i class="ace-icon fa fa-pencil-square-o bigger-120"></i>
                                            </span>
                                          </a>
                                        </li>
                                        <li>
                                          <a href="<?php echo base_url('admin/data_pemberi/hapus/'.$us->id_terima)?>" class="tooltip-error" data-rel="tooltip" title="Delete">
                                            <span class="red">
                                              <i class="ace-icon fa fa-trash-o bigger-120"></i>
                                            </span>
                                          </a>
                                        </li>
                                      </ul>
                                  </div>
                                </div>
                            </td>
                          </tr>
  <!--ace-icon fa fa-pencil-->
                              <?php endforeach ?>
                          </tbody>
                        </table>
                    </div>
                  </div>
                </div>
               </div>                  
              </div>  
            </div>         
            <!-- Modal -->
            <div class="modal fade" id="tambahberi" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
              <div class="modal-dialog" role="document">
                <div class="modal-content">
                  <div class="modal-header">
                    <center><h5 class="modal-title" id="exampleModalLabel">FORM INPUT PEMBERI BANTUAN</h5></center>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div>
                  <div class="modal-body">
                    <form action="<?php echo base_url(). 'admin/data_pemberi/tambah_aksi'?>" method="post" enctype="multipart/form-data">

                      <div class="form-group">
                        <label>Tanggal Terima</label>
                        <input type="date" name="tgl_terima" class="form-control" required="">
                      </div>

                      <div class="form-group">
                        <label>Nama Pemberi</label>
                        <input type="text" name="nama_pemberi" class="form-control" required="">
                      </div>

                      <div class="form-group">
                        <label>Jabatan/Perusahaan</label>
                        <input type="text" name="jabatan" class="form-control" required="">
                      </div>

                      <div class="form-group">
                        <label>Alamat</label>
                        <textarea name="alamat" required="" class="form-control">
                          
                        </textarea> 
                      </div>

                      <div class="form-group">
                        <label>Nama Barang</label>
                        <input type="text" name="nama_barang" class="form-control" required="">
                      </div>

                      <div class="form-group">
                        <label>Jenis Barang</label>
                        <select type="text" name="jenis_barang" class="form-control" required="">
                          <option>MEDIS</option>
                          <option>NON MEDIS</option>
                          <option>BAHAN MAKANAN</option>
                        </select>
                      </div>

                      <div class="form-group">
                        <label>Volume</label>
                        <input type="text" name="volume_beri" class="form-control" required="">
                      </div>

                      <div class="form-group">
                        <label>Satuan</label>
                        <input type="text" name="satuan_beri" class="form-control" required="">
                      </div>

                      <div class="form-group">
                        <label>Harga Satuan</label>
                        <input type="text" name="hrga_satuan" class="form-control">
                      </div>

                      <div class="form-group">
                        <label>Nilai Barang</label>
                        <input type="text" name="nilai_barang" class="form-control" >
                      </div>

                      <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                      </div>
                    </form>
                </div>
              </div>
            </div>
<!--page <table></table>-->
<!-- isi content admin/data_user -->
          <!-- PAGE CONTENT ENDS -->
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.page-content -->
  </div>
</div><!-- /.main-content -->


