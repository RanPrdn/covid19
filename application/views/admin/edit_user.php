<div class="main-content">
  <div class="main-content-inner">
    <div class="breadcrumbs ace-save-state" id="breadcrumbs">
      <ul class="breadcrumb">
        <li>
          <i class="ace-icon fa fa-home home-icon"></i>
          <a href="#">Home</a>
        </li>
        <li class="active">Edit Data user</li>
      </ul><!-- /.breadcrumb -->

      <div class="nav-search" id="nav-search">
        <form class="form-search">
          <span class="input-icon">
            <input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
            <i class="ace-icon fa fa-search nav-search-icon"></i>
          </span>
        </form>
      </div><!-- /.nav-search -->
    </div>
<!--content admin/content-->
    <div class="page-content">
      <div class="page-header"> 
          <small>
            <i class="ace-icon fa fa-angle-double-right"></i>
            Data user administrator bpkad-batam
          </small>
        </h1>
      </div><!-- /.page-header -->

	<div class="content-wrapper">
		<div class="container-fluid">
			<center><h3>EDIT DATA USER</h3></center>
	 	
			<?php foreach ($user as $key => $us) : ?>
				<form method="post"  action="<?php echo base_url(). 'admin/data_user/update' ?>">
					<div class="form-group">
						<label>Username</label>
						<input type="hidden" name="id_user" class="form-control" value="<?php echo $us->id_user?>">
						<input type="text" name="username" class="form-control" value="<?php echo $us->username?>">
					</div>

					<div class="form-group">
						<label>Password</label>
						<input type="text" name="pwd" class="form-control" value="<?php echo $us->pwd?>">
					</div>

					<div class="form-group">
						<label>Level</label>
							<select type="text" name="level" class="form-control" value="<?php echo $us->level?>">
					              <option>Operator</option>
					              <option>Admin</option>
					              <option>Pegawai</option>
		            		</select>
					</div>

					<div class="form-group"> 
						<label>Bagian</label>
							<select type="text" name="bagian" class="form-control" value="<?php echo $us->bagian?>">
				              <option>SDM</option>
				              <option>Aset</option>
				              <option>Anggaran</option>
				            </select>
					</div>

					<button type="submit" class="btn btn-primary btn-sm mt-2"> Simpan</button>
				</form>

			<?php endforeach;?>
		</div>
	</div>
</div>