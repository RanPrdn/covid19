<div style="background: url(<?php echo base_url('assets/images/background.jpg'); ?>);background-repeat: no-repeat, repeat;background-size:100% 100%;">
<br>
<br>
<br>
<br>
<center>
    <h2 class="container" data-aos="zoom-in-down"
    data-aos-delay="50"
    data-aos-duration="500"
    data-aos-mirror="true"
    data-aos-anchor-placement="top-center" style="font-family: 'Russo One', cursive">DOWNLOAD FILE <span style="font-size: 40px;color:#ff7700">SURAT EDARAN MENTERI KEUANGAN</span></h2><br>
</center>
<div class="container" data-aos="flip-down"
    data-aos-delay="50"
    data-aos-duration="2000"
    data-aos-mirror="true"
    data-aos-anchor-placement="top-center">

	<div class="card bg-light text-dark">
        <div class="card-body">
           <table id="semk" class="table table-bordered table-striped" style="width: 100%">
				<thead>
					<tr>
						<th>Tahun</th>
						<th>Surat Edaran Menteri Keuangan</th>
						<th>Uraian</th>
						<th>Download</th>
					</tr>
				</thead>
				<tbody>
					<?php
                    $query = $this->db->query('SELECT * FROM keuda WHERE ktg_keuda LIKE "%Surat Edaran Menteri Keuangan%"');

                        foreach ($query->result() as $row)
                        {?>
                        <tr>
                        		<td><?php echo $row->tahun;?></td>
                                <td><?php echo $row->nm_keuda;?></td>
                              	<td><?php echo $row->uraian_keuda; ?></td>
								<td><center><a href="<?php echo base_url('uploads/').$row->file;?>" class="btn btn-success btn-sm" target="_blank">View</a></center></td>
						</tr>
						<?php
							}
							?>
				</tbody>
			</table>

        </div>
    </div>
</div>
<br>
<br>
    
</div>

