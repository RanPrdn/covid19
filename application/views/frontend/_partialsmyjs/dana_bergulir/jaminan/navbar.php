<!--MENU START-->

<nav class="navbar navbar-expand-lg navbar-custom" id="myNavbar">
    <!-- Brand/logo -->

  <a class="" href="<?php echo base_url()?>">
   <img class="img-fluid mobile-hide" src="<?php echo base_url()?>assets/images/logobpkad.png" alt="logo">
    <img class="img-fluid desktop-hide" src="<?php echo base_url()?>assets/images/logobpkadmobile.png" alt="logo">
  </a>
   
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCustom">
        <i style="font-size: 19px" class="fa fa-bars fa-lg py-1 text-white"></i>
    </button>

   <div style="font-family: 'Roboto Condensed', sans-serif;" class="navbar-collapse collapse justify-content-end" id="navbarCustom">
       <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link" href="<?php echo base_url('welcome') ?>">BERANDA</a>
            </li>

             <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                 PROFIL
                </a>
                <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                    <li><a class="dropdown-item" href="<?php echo base_url('katasambutan') ?>">Kata Sambutan</a></li>
                    <li><a class="dropdown-item" href="<?php echo base_url('visimisi') ?>">Visi Misi</a></li>
                    <li><a class="dropdown-item" href="<?php echo base_url('profilkeanggotaan') ?>">Profil Pejabat</a></li>
                </ul>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?php echo base_url('strukturorganisasi') ?>">STRUKTUR ORGANISASI</a>
            </li>

            <li class="nav-item dropdown active">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
             DANA BERGULIR
            </a>
            <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                <li class="dropdown-submenu"><a class="dropdown-item dropdown-toggle" href="#">Persyaratan dan Kriteria</a>
                    <ul class="dropdown-menu">
                      <li><a class="dropdown-item" href="<?php echo base_url('koperasi') ?>">Koperasi</a></li>
                      <li><a class="dropdown-item" href="<?php echo base_url('usahamikro') ?>">Usaha Mikro</a></li>
                    </ul>
                </li>
              

              <li class="dropdown-submenu"><a class="dropdown-item dropdown-toggle" href="#">Tujuan dan Sasaran</a>
                    <ul class="dropdown-menu">
                  <li><a class="dropdown-item" href="<?php echo base_url('tujuandanabergulir') ?>">Tujuan</a></li>
                  <li><a class="dropdown-item" href="<?php echo base_url('sasarandanabergulir') ?>">Sasaran</a></li>
                </ul>
                </li>
                <li class="dropdown-submenu"><a class="dropdown-item dropdown-toggle" href="#">Pinjaman</a>
                    <ul class="dropdown-menu">
                      <li><a class="dropdown-item" href="<?php echo base_url('besaranjasapinjaman') ?>">Besaran Jasa Pinjaman</a></li>
                      <li><a class="dropdown-item" href="<?php echo base_url('besaranpinjamanyangdiberikan') ?>">Besaran Pinjaman</a></li>
                      <li><a class="dropdown-item" href="<?php echo base_url('jaminan') ?>">Jaminan</a></li>
                      <li><a class="dropdown-item" href="<?php echo base_url('waktupinjaman') ?>">Waktu Pinjaman</a></li>
                    </ul>
                </li>
                <li><a class="dropdown-item" href="<?php echo base_url('Sop') ?>">Standar Operasi Pelayanan</a></li>
                <li class="dropdown-submenu"><a class="dropdown-item dropdown-toggle" href="#">Lampiran</a>
                    <ul class="dropdown-menu">
                      <li><a class="dropdown-item" href="<?php echo base_url('Download') ?>">Download</a></li>
                    </ul>
                </li>
            </ul>
        </li>

        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
             GALERI
            </a>
            <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                <li><a class="dropdown-item" href="<?php echo base_url('Berita') ?>">Berita</a></li>
                 <li><a class="dropdown-item" href="<?php echo base_url('Video') ?>">Video</a></li>
                 <li><a class="dropdown-item" href="<?php echo base_url('TransaksiNonTunai') ?>"><b>Transaksi Non-Tunai</b></a></li>
              </ul>
     
        <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                 REGULASI KEUDA
                </a>
                <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">

                    <li><a class="dropdown-item" href="<?php echo base_url('undangundang') ?>">Undang Undang 
                                                    <?php
                          $query = $this->db->query("SELECT COUNT( * ) as total FROM keuda WHERE ktg_keuda ='Undang Undang'");
                          $row = $query->row();
                          if (isset($row))
                          {       
                                  echo("<b> (");echo $row->total; echo(")</b>");
                          }
                          ?>
                    <li><a class="dropdown-item" href="<?php echo base_url('peraturanpemerintah') ?>">Peraturan Pemerintah   <?php
                          $query = $this->db->query("SELECT COUNT( * ) as total FROM keuda WHERE ktg_keuda ='Peraturan Pemerintah'");

                          $row = $query->row();

                          if (isset($row))
                          {       
                                  echo("<b> (");echo $row->total; echo(")</b>");
                          }
                          ?></a></li>
                    <li><a class="dropdown-item" href="<?php echo base_url('peraturanpresiden') ?>">Peraturan Presiden   <?php
                          $query = $this->db->query("SELECT COUNT( * ) as total FROM keuda WHERE ktg_keuda ='Peraturan Presiden'");

                          $row = $query->row();

                          if (isset($row))
                          {       
                                  echo("<b> (");echo $row->total; echo(")</b>");
                          }
                          ?></a></li>

                    <li><a class="dropdown-item" href="<?php echo base_url('instruksipresiden') ?>">Instruksi Presiden   <?php
                          $query = $this->db->query("SELECT COUNT( * ) as total FROM keuda WHERE ktg_keuda ='Instruksi Presiden'");

                          $row = $query->row();

                          if (isset($row))
                          {       
                                  echo("<b> (");echo $row->total; echo(")</b>");
                          }
                          ?></a></li>

                    <li class="dropdown-submenuright"><a class="dropdown-item dropdown-toggle" href="#">Peraturan Menteri</a>
                        <ul class="dropdown-menu dropdown-menu">
                            <li><a class="dropdown-item" href="<?php echo base_url('peraturanmenterikeuangan') ?>">Peraturan Menteri Keuangan   <?php
                          $query = $this->db->query("SELECT COUNT( * ) as total FROM keuda WHERE ktg_keuda ='Pearaturan Menteri Keuangan'");

                          $row = $query->row();

                          if (isset($row))
                          {       
                                  echo("<b> (");echo $row->total; echo(")</b>");
                          }
                          ?></a></li>
                            <li><a class="dropdown-item" href="<?php echo base_url('peraturanmenteridalamnegri') ?>">Peraturan Menteri Dalam Negeri   <?php
                          $query = $this->db->query("SELECT COUNT( * ) as total FROM keuda WHERE ktg_keuda ='Peraturan Menteri Dalam Negeri'");

                          $row = $query->row();

                          if (isset($row))
                          {       
                                  echo("<b> (");echo $row->total; echo(")</b>");
                          }
                          ?></a></li>
                        </ul>
                    </li>
                    <li><a class="dropdown-item" href="<?php echo base_url('peraturandaerah') ?>">Peraturan Daerah   <?php
                          $query = $this->db->query("SELECT COUNT( * ) as total FROM keuda WHERE ktg_keuda ='Peraturan Daerah'");

                          $row = $query->row();

                          if (isset($row))
                          {       
                                  echo("<b> (");echo $row->total; echo(")</b>");
                          }
                          ?></a></li>
                    <li><a class="dropdown-item" href="<?php echo base_url('peraturanwalikota') ?>">Peraturan Walikota   <?php
                          $query = $this->db->query("SELECT COUNT( * ) as total FROM keuda WHERE ktg_keuda ='Peraturan Walikota'");

                          $row = $query->row();

                          if (isset($row))
                          {       
                                  echo("<b> (");echo $row->total; echo(")</b>");
                          }
                          ?></a></li>
                    <li><a class="dropdown-item" href="<?php echo base_url('keputusanwalikota') ?>">Keputusan Walikota   <?php
                          $query = $this->db->query("SELECT COUNT( * ) as total FROM keuda WHERE ktg_keuda ='Keputusan Walikota'");

                          $row = $query->row();

                          if (isset($row))
                          {       
                                  echo("<b> (");echo $row->total; echo(")</b>");
                          }
                          ?></a></li>

                    <li class="dropdown-submenuright"><a class="dropdown-item dropdown-toggle" href="#">Surat Edaran</a>
                        <ul class="dropdown-menu dropdown-menu">
                            <li><a class="dropdown-item" href="<?php echo base_url('suratedaranmenteridalamnegri') ?>">Surat Edaran Menteri Dalam Negeri   <?php
                          $query = $this->db->query("SELECT COUNT( * ) as total FROM keuda WHERE ktg_keuda ='Surat Edaran Dalam Negeri'");

                          $row = $query->row();

                          if (isset($row))
                          {       
                                  echo("<b> (");echo $row->total; echo(")</b>");
                          }
                          ?></a></li>
                            <li><a class="dropdown-item" href="<?php echo base_url('suratedaranmenterikeuangan') ?>">Surat Edaran Menteri Keuangan   <?php
                          $query = $this->db->query("SELECT COUNT( * ) as total FROM keuda WHERE ktg_keuda ='Surat Edaran Menteri Keuangan'");

                          $row = $query->row();

                          if (isset($row))
                          {       
                                  echo("<b> (");echo $row->total; echo(")</b>");
                          }
                          ?></a></li>

                           <li><a class="dropdown-item" href="<?php echo base_url('suratedaranwalikota') ?>">Surat Edaran Walikota   <?php
                          $query = $this->db->query("SELECT COUNT( * ) as total FROM keuda WHERE ktg_keuda ='Surat Edaran Walikota'");

                          $row = $query->row();

                          if (isset($row))
                          {       
                                  echo("<b> (");echo $row->total; echo(")</b>");
                          }
                          ?></a></li>
                        </ul>
                    </li>
                   
                </ul>
            </li>

            <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
             TRANSPARANSI
            </a>
            <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
              <li><a class="dropdown-item" href="<?php echo base_url('renstra') ?>">Renstra <?php
                          $query = $this->db->query("SELECT COUNT( * ) as total FROM renstra");

                          $row = $query->row();

                          if (isset($row))
                          {       
                                  echo("<b> (");echo $row->total; echo(")</b>");
                          }
                          ?></a></li>
             
             <li><a class="dropdown-item" href="<?php echo base_url('Apbd') ?>">Ringkasan APBD <?php
                          $query = $this->db->query("SELECT COUNT( * ) as total FROM apbd");

                          $row = $query->row();

                          if (isset($row))
                          {       
                                  echo("<b> (");echo $row->total; echo(")</b>");
                          }
                          ?></a></li>
                <li> <a class="dropdown-item" href="<?php echo base_url('anggaran') ?>">Realisasi Anggaran <?php
                          $query = $this->db->query("SELECT COUNT( * ) as total FROM anggaran");

                          $row = $query->row();

                          if (isset($row))
                          {       
                                  echo("<b> (");echo $row->total; echo(")</b>");
                          }
                          ?></a></li>
                 <li><a class="dropdown-item" href="<?php echo base_url('hibah') ?>">Realisasi Hibah <?php
                          $query = $this->db->query("SELECT COUNT( * ) as total FROM hibah");

                          $row = $query->row();

                          if (isset($row))
                          {       
                                  echo("<b> (");echo $row->total; echo(")</b>");
                          }
                          ?></a></li>




                <li class="dropdown-submenuright"><a class="dropdown-item dropdown-toggle" href="#">LKPJ</a>
                        <ul class="dropdown-menu dropdown-menu">
                            <li><a class="dropdown-item" href="<?php echo base_url('lkpjATA') ?>">Akhir Tahun Anggaran  <?php
                          $query = $this->db->query("SELECT COUNT( * ) as total FROM lkpj WHERE bagian LIKE '%Akhir Tahun Anggaran%'");

                          $row = $query->row();

                          if (isset($row))
                          {       
                                  echo("<b> (");echo $row->total; echo(")</b>");
                          }
                          ?></a></li>
                            <li><a class="dropdown-item" href="<?php echo base_url('lkpjAMJ') ?>">Akhir Masa Jabatan <?php
                          $query = $this->db->query("SELECT COUNT( * ) as total FROM lkpj WHERE bagian LIKE '%Akhir Masa Jabatan%'");

                          $row = $query->row();

                          if (isset($row))
                          {       
                                  echo("<b> (");echo $row->total; echo(")</b>");
                          }
                          ?></a></li>
                        </ul>
                </li>
                <li class="dropdown-submenuright"><a class="dropdown-item dropdown-toggle" href="#">LPPD</a>
                        <ul class="dropdown-menu dropdown-menu">
                            <li><a class="dropdown-item" href="<?php echo base_url('LKUNAudit') ?>">LK UN AUDIT <?php
                          $query = $this->db->query("SELECT COUNT( * ) as total FROM lkpd WHERE bagian LIKE '%LK UN AUDIT%'");

                          $row = $query->row();

                          if (isset($row))
                          {       
                                  echo("<b> (");echo $row->total; echo(")</b>");
                          }
                          ?></a></li>
                            <li><a class="dropdown-item" href="<?php echo base_url('LKaudit') ?>">LK AUDIT <?php
                          $query = $this->db->query("SELECT COUNT( * ) as total FROM lkpd WHERE bagian LIKE '%LK AUDIT%'");

                          $row = $query->row();

                          if (isset($row))
                          {       
                                  echo("<b> (");echo $row->total; echo(")</b>");
                          }
                          ?></a></li>
                            <li><a class="dropdown-item" href="<?php echo base_url('OpiniBPK') ?>">OPINI BPK <?php
                          $query = $this->db->query("SELECT COUNT( * ) as total FROM lkpd WHERE bagian LIKE '%OPINI BPK%'");

                          $row = $query->row();

                          if (isset($row))
                          {       
                                  echo("<b> (");echo $row->total; echo(")</b>");
                          }
                          ?></a></li>
                        </ul>
                </li>              
            </ul>
        </li>

            <li class="nav-item">
              
                <a class="nav-link" href="#kontak"  data-toggle="collapse" data-target=".navbar-collapse">KONTAK</a>
            </li>

        </ul>
    </div>
</nav>
<!-- MENU END -->