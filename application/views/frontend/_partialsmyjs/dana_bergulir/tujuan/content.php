<div style="background: url(<?php echo base_url('assets/images/background.jpg'); ?>);background-repeat: no-repeat, repeat;background-size:cover;">
<br>
<br>
<br>
<br>
<center>
    <h2 class="container" data-aos="fade-in"
    data-aos-delay="50"
    data-aos-duration="500"
    data-aos-mirror="true"
    data-aos-anchor-placement="top-center" style="font-family: 'Russo One'; font-size: 40px;color:#ff7700">TUJUAN <span style="font-family: 'Russo One';font-size: 35px;color:#000">PINJAMAN DANA BERGULIR</span><br></h2>
</center>
<div class="container" data-aos="fade-in"
    data-aos-delay="50"
    data-aos-duration="2000"
    data-aos-mirror="true"
    data-aos-anchor-placement="top-center">

    <div class="card bg-light text-dark" style="font-family: 'Roboto'">
        <div class="card-body">
        <ol type="a">
            <li>Menstimulasi pengembangan permodalan koperasi dan usaha mikro.</li>
            <li>Mendukung permodalan koperasi dan usaha mikro di berbagai sektor produktif yang belum tersedia pembiayaannya secara  memadai dari lembaga keuangan yang ada.</li>
            <li>Mengembangkan usaha koperasi dan usaha mikro sektor produktif yang bernilai tambah tinggi, menyerap tenaga kerja sebagai upaya peningkatan pendapatan.</li>
        </ol>
          
    </div>
</div>
<br>
<br>
    
</div>
</div>