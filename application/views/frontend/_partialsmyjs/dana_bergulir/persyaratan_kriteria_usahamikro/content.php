<div style="background: url(<?php echo base_url('assets/images/background.jpg'); ?>);background-repeat: no-repeat, repeat;background-size: cover;">
<br>
<br>
<br>
<br>
<center>
    <h2 class="container" data-aos="fade-in"
    data-aos-delay="50"
    data-aos-duration="500"
    data-aos-mirror="true"
    data-aos-anchor-placement="top-center" style="font-family: 'Russo One', cursive">KRITERIA DAN PERSYARATAN  <span style="font-size: 40px;color:#ff7700">USAHA MIKRO</span></h2><br>
</center>
<div class="container" data-aos="fade-in"
    data-aos-delay="50"
    data-aos-duration="2000"
    data-aos-mirror="true"
    data-aos-anchor-placement="top-center" style="font-family: 'Roboto'">

    <div class="card bg-light text-dark">
        <div class="card-body">
            <h3>Kriteria Usaha Mikro :</h3>
        <ol type="a">
            <li>Penduduk kota Batam.</li>
            <li>Berusia minimal 17 (tujuh belas) tahun dan maksimal 60 (enam puluh) tahun.</li>
            <li>Tidak berprofesi sebagai Pegawai Negri Sipil (PNS) aktif, Tentara Nasional Indonesia (TNI) aktif, Polisi Republik Indonesia (POLRI) aktif, Anggota DPRD aktif dan Suami atau Isteri Anggota DPRD aktif.</li>
            <li>Memiliki usaha produktif dan jasa yang layak dikembangkan;</li>
            <li>Telah mengajukan usaha minimal 6 (enam) bulan; </li>
            <li>Bertempat tinggal dipemukiman resmi; dan</li>
            <li>Untuk usaha mikro memiliki aset sampai dengan <b>Rp.50.000.000,- (lima puluh juta rupiah)</b> tidak termasuk tanah dan bangunan serta memiliki omset sampai dengan <b>Rp.300.000.000,- (tiga ratus juta rupiah)</b> pertahun, yang dibuktikan dengan neraca dan rincian rugi laba;</li>
        </ol>
            <br>
            <h3>Persyaratan Usaha Mikro :</h3>
        <ol type="a">
            <li>Pemohon mengajukan proposal dan mengisi formulir;</li>
            <li>Melampirkan foto copy KTP suami dan isteri yang masih berlaku dan Kartu Keluarga (KK);</li>
            <li>Melampirkan surat keterangan domisili dari kelurahan setempat;</li>
            <li>foto copy surat izin usaha mikro dari kecamatan (IUMK);</li>
            <li>Pas foto terbaru suami dan isteri ukuran 3x4 cm sebanyak 2 (dua) lembar;</li>
            <li>Melampirkan foto copy surat nikah bagi yang sudah menikah;</li>
            <li>Melampirkan surat persetujuan suami/isteri;</li>
            <li>Melampirkan Pencatatan total penerima dan pengeluaran usaha 3 (tiga) bulan terakhir;</li>
            <li>Melampirkan foto usaha;</li>
            <li>Melampirkan foto copy dokumen jaminan;</li>
            <li>Melampirkan foto jaminan; dan</li>
            <li>Melampirkan foto copy rekening Bank yang ditunjuk;</li>
        </ol>
    </div>
</div>
<br>
<br>
    
</div>
</div>