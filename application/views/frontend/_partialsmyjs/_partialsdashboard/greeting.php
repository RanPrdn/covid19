<!--KATA SAMBUTAN PAK KABAN-->
<div style="background-color: #ffffff;">
	<div class="container" style="font-family: 'Roboto Condensed', sans-serif;" data-aos="zoom-in-up"
    data-aos-delay="50"
    data-aos-duration="1000"
    data-aos-mirror="true"
    data-aos-anchor-placement="top-center">
		<br>
		<p>
		<center><img style="max-width: 25%; " class="img-fluid" src="<?php echo base_url()?>assets/images/kabidnew2.png"></center>
		</p>
		<center><b>Selamat Datang di Website Badan Pengelola Keuangan dan Aset Daerah Kota Batam.</b></center>
		<p style="text-align: justify;">
			Website ini dimaksudkan sebagai sarana publikasi untuk memberikan informasi dan gambaran Badan Pengelola Keuangan dan Aset Daerah (BPKAD) Kota Batam dalam melaksanakan Pengelolaan Keuangan dan Aset Daerah. Melalui keberadaan website ini kiranya masyarakat dapat mengetahui seluruh informasi tentang Kebijakan Pemerintah Kota Batam didalam Pengelolaan sektor Anggaran Keuangan & Aset Daerah di wilayah Pemerintah Kota Batam.</p>
		<p>
			<center><b>Kepala Badan Pengelolaan Keuangan dan Aset<br>Daerah Kota Batam</b></center>
		</p>
		<br>
	</div>

</div>
<!--KATA SAMBUTAN PAK KABAN END-->