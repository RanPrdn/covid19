<div style="background: url(<?php echo base_url('assets/images/background.jpg'); ?>);background-repeat: no-repeat, repeat;background-size:cover;">
<br>
<br>
<br>
<br>
<center>
    <h2 class="container" data-aos="zoom-in-down"
    data-aos-delay="50"
    data-aos-duration="500"
    data-aos-mirror="true"
    data-aos-anchor-placement="top-center" style="font-family: Russo One">Download <span style="color:#ff7700;font-size: 40px">Lampiran</span></h2><br>
</center>
<div class="container" data-aos="flip-down"
    data-aos-delay="50"
    data-aos-duration="2000"
    data-aos-mirror="true"
    data-aos-anchor-placement="top-center">

	<div class="card bg-light text-dark">
        <div class="card-body">
            
<div class="container-fluid" style="padding-top:20px">
                      
            <div class="container">
                <table class="table table-hover">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Keterangan</th>
                        <th>Download</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>1</td>
                        <td>Data Calon Mitra Binaan</td>
                        <td> <a href="<?php echo base_url('assets/download/form data calon mitra binaan.pdf'); ?>" target="_blank" class="btn btn-success btn-sml"> View </a></td>
                      </tr>
                      <tr>
                        <td>2</td>
                        <td>Permohonan Pinjaman Dana Bergulir</td>
                        <td><a href="<?php echo base_url('assets/download/form permohonan pinjaman.pdf'); ?>" target="_blank" class="btn btn-success btn-sml"> View </a></td>
                      </tr>
                      <tr>
                        <td>3</td>
                        <td>Surat Pernyataan</td>
                        <td><a href="<?php echo base_url('assets/download/form surat pernyataan.pdf'); ?>" target="_blank" class="btn btn-success btn-sml"> View </a></td>
                      </tr>
                      <tr>
                        <td>4</td>
                        <td>Surat Kuasa dan Persetujuan</td>
                        <td><a href="<?php echo base_url('assets/download/form surat kuasa dan persetujuan.pdf'); ?>" target="_blank" class="btn btn-success btn-sml"> View </a></td>
                      </tr>
                      <tr>
                        <td>5</td>
                        <td>Surat Persetujuan & Kuasa Suami/ Istri</td>
                        <td><a href="<?php echo base_url('assets/download/form surat persetujuan & kuasa suami atau isteri.pdf'); ?>" target="_blank" class="btn btn-success btn-sml"> View </a></td>
                      </tr>
                      <tr>
                        <td>6</td>
                        <td>UPT - Pengelolaan Dana Bergulir Tabel Angsuran</td>
                        <td><a href="<?php echo base_url('assets/download/form tabel angsuran.pdf'); ?>" target="_blank" class="btn btn-success btn-sml"> View </a></td>
                      </tr>
                    </tbody>
                </table>
        </div>
        </div>
    </div>
</div>
</div>
<br>
<br>
