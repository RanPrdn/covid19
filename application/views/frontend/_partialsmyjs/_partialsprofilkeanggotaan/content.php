<style type="text/css">

.card-horizontal {
    display: flex;
    flex: 1 1 auto;
}

</style>

<div style="background: url(<?php echo base_url('assets/images/background.jpg'); ?>);background-repeat: no-repeat, repeat;background-size:cover;">

<br>
<br>
<br>
<br>
<center>
    <h2 class="container" data-aos="fade-in"
    data-aos-delay="50"
    data-aos-duration="500"
    data-aos-mirror="true"
    data-aos-anchor-placement="top-center">PROFIL KEANGGOTAAN</h2><br>
</center>
<div class="container" data-aos="fade-in"
    data-aos-delay="50"
    data-aos-duration="2000"
    data-aos-mirror="true"
    data-aos-anchor-placement="top-center">

	<div class="card bg-light text-dark">
        <div class="card-body">
           <!--Tampilan-->
            <ul class="nav nav-tabs" id="myTab" role="tablist">
              <li class="nav-item">
                <a class="nav-link active" id="kepala-tab" data-toggle="tab" href="#kepala" role="tab" aria-controls="kepala"
                  aria-selected="true">Kepala BPKAD</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" id="sekretaris-tab" data-toggle="tab" href="#sekretaris" role="tab" aria-controls="sekretaris"
                  aria-selected="false">Sekretaris</a>
              </li>
               <li class="nav-item">
                <a class="nav-link" id="eselon3-tab" data-toggle="tab" href="#eselon3" role="tab" aria-controls="eselon3"
                  aria-selected="false">Eselon III</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" id="eselon4-tab" data-toggle="tab" href="#eselon4" role="tab" aria-controls="eselon4"
                  aria-selected="false">Eselon IV</a>
              </li>
            </ul>
            <div class="tab-content" id="myTabContent" style="font-family: Roboto">
              <div class="tab-pane fade show active" id="kepala" role="tabpanel" aria-labelledby="kepala-tab">
                <div class="row">
                    <div class="col-lg-3">
                            <img class="img-fluid" src="<?php echo base_url('assets/images/profil/kepala.jpg')?>">
                    </div>
                    <div class="col-lg-9">
                        <br>
                        <ul>
                            <li>
                                <b>NAMA</b> <br> ABD. MALIK, SE, M.Si
                            </li>
                            <li>
                                <b>PANGKAT/ GOL</b><br> Pembina Utama Muda (IV/c)
                            </li>
                            <li>
                                <b>TEMPAT DAN TANGGAL LAHIR </b><br> Kota Lama, Kampar 27 Juli 1966
                            </li>
                            <li>
                                <b>AGAMA</b><br> ISLAM
                            </li>

                        </ul> 
                           
                    </div>
                </div>
              </div>
              <div class="tab-pane fade" id="sekretaris" role="tabpanel" aria-labelledby="sekretaris-tab">

                <div class="row">
                    <div class="col-lg-3">
                            <img class="img-fluid" src="<?php echo base_url('assets/images/profil/sekretaris.jpg')?>">
                    </div>
                    <div class="col-lg-9">
                        <br>
                        <ul>
                            <li>
                                <b>NAMA</b> <br> Drs. FAISAL RIZA, M.Ec. Dev
                            </li>
                            <li>
                                <b>PANGKAT/ GOL</b><br> Pembina Tk.I (IV/b)
                            </li>
                            <li>
                                <b>TEMPAT DAN TANGGAL LAHIR </b><br> Pekanbaru, 10 Oktober 1975
                            </li>
                            <li>
                                <b>AGAMA</b><br> ISLAM
                               </li>

                        </ul> 
                           
                    </div>
                </div>
            </div>

            <div class="tab-pane fade" id="eselon3" role="tabpanel" aria-labelledby="eselon3-tab">
               
                <div class="row">
                    <?php
                                $data = $this->db->query("SELECT * FROM profile WHERE pangkat LIKE '%eselon3%'") ;
                                foreach ($data->result_array() as $row):
                                $id_profil=$row['id_profile'];
                                $nama=$row['nm_profile'];
                                $nip=$row['nip'];
                                $jabatan=$row['jabatan'];
                                $foto=$row['foto'];
                                ?>
        <div class="col-lg-6">
            <div class="card">

                <div class="card-horizontal">
                    <div class="img-square-wrapper">
                        <br>
                        <img  id="<?php echo $id_profil ?>" class="img-fluid mobile-hide btn View_Data" src="<?php echo base_url().'assets/images/profil/'.$foto;?>" style="width:60%; margin-bottom: 50px;margin-left: 100px">
                        <img  id="<?php echo $id_profil ?>" class="img-fluid desktop-hide btn View_Data" src="<?php echo base_url().'assets/images/profil/'.$foto;?>" style="width:100%; margin-bottom: 50px; margin-left: 25px;margin-top:15px"> 
                    </div>
                    <div class="card-body">
                        <p class="card-text" style="margin-left: 200px">
                           
                        <ul>
                            <li>
                                <b>NAMA</b> <br> <?php echo $nama; ?>
                            </li>
                            <li>
                                <b>NIP</b><br> <?php echo $nip; ?>
                            </li>
                            <li>
                                <b>Jabatan</b><br> <?php echo $jabatan; ?>
                            </li>
                        </ul>
                          <hr> </p>
                    </div>
                </div>
                
            </div>
        </div>
    <?php endforeach?>
                     
                </div>
            </div>
           <!--End Tampilan-->


           <div class="tab-pane fade" id="eselon4" role="tabpanel" aria-labelledby="eselon4-tab">

                <div class="row">
                    <?php
                                $data = $this->db->query("SELECT * FROM profile WHERE pangkat LIKE '%eselon4%'") ;
                                foreach ($data->result_array() as $row):
                                $id_profil=$row['id_profile'];
                                $nama=$row['nm_profile'];
                                $nip=$row['nip'];
                                $jabatan=$row['jabatan'];
                                $foto=$row['foto'];
                                ?>
        <div class="col-lg-6">
            <div class="card">

                <div class="card-horizontal">
                    <div class="img-square-wrapper">
                        <br>

                        <img id="<?php echo $id_profil ?>" style="width:60%; margin-bottom: 50px;margin-left: 100px" src="<?php echo base_url().'assets/images/profil/'.$foto;?>" class="thumbnail btn View_Data mobile-hide img-fluid">


                       <!--  <a class='btn' href='#' data-id='".$row->id_profile."' data-toggle='modal' data-target='#myModal'><img id="myImg"  data-u="image" class="img-fluid mobile-hide" src="<?php echo base_url().'assets/images/profil/'.$foto;?>" data-id='".$row->id_profile."' style="width:60%; margin-bottom: 50px;margin-left: 100px"></a> -->

                        <img  id="<?php echo $id_profil ?>" class="img-fluid desktop-hide btn View_Data" src="<?php echo base_url().'assets/images/profil/'.$foto;?>" style="width:100%; margin-bottom: 50px; margin-left: 25px;margin-top:15px"> 
                    </div>
                    <div class="card-body">
                        <p class="card-text" style="margin-left: 200px">
                           
                        <ul>
                            <li>
                                <b>NAMA</b> <br> <?php echo $nama; ?>
                            </li>
                            <li>
                                <b>NIP</b><br> <?php echo $nip; ?>
                            </li>
                            <li>
                                <b>Jabatan</b><br> <?php echo $jabatan; ?>
                            </li>
                        </ul>
                          <hr> </p>
                    </div>
                </div>
                
            </div>
        </div>
    <?php endforeach?>
                </div>
            </div>
       </div>
    </div>
</div>
<br>
<br>
    
</div>
</div>


<!-- view Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true" style="margin-top: -20px;">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
          <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">Foto Profil</h4><button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          </div>
          <div class="modal-body">
         
            <div id="View_result"></div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-warning btn-danger" data-dismiss="modal">Tutup</button>
          </div>
        </div>
      </div>
    </div>>


    <!-- jQuery JS CDN -->
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script> 
 
    <!-- Bootstrap JS CDN -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    

<script>
$(document).on("click", ".btn", function () {
    var ViewData = $(this).attr('id');
                // Start AJAX function
                $.ajax({
                    // Path for controller function which fetches selected phone data
                    url: "<?php echo base_url() ?>View/get_view_result",
                    // Method of getting data
                    method: "POST",
                    // Data is sent to the server
                    data: {ViewData:ViewData},
                    // Callback function that is executed after data is successfully sent and recieved
                    success: function(data){
                        // Print the fetched data of the selected phone in the section called #phone_result 
                        // within the Bootstrap modal
                        $('#View_result').html(data);
                        // Display the Bootstrap modal
                        $('#myModal').modal('show');
                    }

                });
                // End AJAX function
});
</script>