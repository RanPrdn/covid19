 <style>
body {font-family: Arial, Helvetica, sans-serif;}

#myImg {
  border-radius: 5px;
  cursor: pointer;
  transition: 0.3s;
}

#myImg:hover {opacity: 0.7;}

#myImg2 {
  border-radius: 5px;
  cursor: pointer;
  transition: 0.3s;
}

#myImg2:hover {opacity: 0.7;}

#myImg3 {
  border-radius: 5px;
  cursor: pointer;
  transition: 0.3s;
}

#myImg3:hover {opacity: 0.7;}

/* The Modal (background) */
.modal {
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 1; /* Sit on top */
  padding-top: 100px; /* Location of the box */
  left: 0;
  top: 0;
  width: 100%; /* Full width */
  height: 100%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
}

/* Modal Content (image) */
.modal-content {
  margin: auto;
  display: block;
  width: 80%;
  max-width: 700px;
}

/* Add Animation */
.modal-content, #caption {  
  -webkit-animation-name: zoom;
  -webkit-animation-duration: 0.6s;
  animation-name: zoom;
  animation-duration: 0.6s;
}

@-webkit-keyframes zoom {
  from {-webkit-transform:scale(0)} 
  to {-webkit-transform:scale(1)}
}

@keyframes zoom {
  from {transform:scale(0)} 
  to {transform:scale(1)}
}

/* The Close Button */
.close {
  position: absolute;
  top: 15px;
  right: 35px;
  color: #f1f1f1;
  font-size: 40px;
  font-weight: bold;
  transition: 0.3s;
}

.close:hover,
.close:focus {
  color: #bbb;
  text-decoration: none;
  cursor: pointer;
}

.close1 {
  position: absolute;
  top: 15px;
  right: 35px;
  color: #f1f1f1;
  font-size: 40px;
  font-weight: bold;
  transition: 0.3s;
}

.close1:hover,
.close1:focus {
  color: #bbb;
  text-decoration: none;
  cursor: pointer;
}

.close2 {
  position: absolute;
  top: 15px;
  right: 35px;
  color: #f1f1f1;
  font-size: 40px;
  font-weight: bold;
  transition: 0.3s;
}

.close2:hover,
.close2:focus {
  color: #bbb;
  text-decoration: none;
  cursor: pointer;
}

/* 100% Image Width on Smaller Screens */
@media only screen and (max-width: 700px){
  .modal-content {
    width: 100%;
  }
}
</style>
    
<style type="text/css">


      /* Make the image fully responsive */
  .carousel-inner img {
      width: 100%;
      height: 100%;

  }
  
.contoh-link:link,
.contoh-link:active,
.contoh-link:visited{

    color: #000;
    font-size: 12pt;
    text-decoration: none;
    font-family: sans-serif;    
    font-family: 'Roboto Condensed', sans-serif; text-align: left;
}
 
.contoh-link:hover{
    background-color: grey;
}
</style>
<div style="background: url(<?php echo base_url('assets/images/background.jpg'); ?>);background-repeat: no-repeat, repeat;background-size:100% 100%;">
    <br><br>
    <div class="container">
       <div class="row">
        <div class="col-lg-8">

    <!--<script src="https://dl.dropboxusercontent.com/u/226227/jquery-1.9.1.min.js"></script>-->
    <script src="https://orquestra.upc.edu/orquestra/ca/shared/youmax/jquery/jquery-1-9-1-min.js/@@download/file/jquery-1.9.1.min.js"></script>
    <script src="https://orquestra.upc.edu/orquestra/ca/shared/youmax/js/youmax.js/@@download/file/youmax.js"></script>
    <link rel="stylesheet" href="https://orquestra.upc.edu/orquestra/ca/shared/youmax/css/youmax.css/@@download/file/youmax.css"/>

    <style>

      #youmax{width:100% !important;}
      #youmax-video-list-div{background-color:#02012a !important;}
      #youmax-tabs{background-color:#02012a !important;}
      #youmax-header{background-color:orange !important;}
      .youmax-video-list-title{padding:15px;color:black !important;}
      .youmax-video-list-views{color:green !important;}


    </style>



<div id="youmax"></div>
    <script>
        $('#youmax').youmax({
         apiKey:'AIzaSyD3ncp2kwM9Fb4ta0CK_ppTaZ0kztE5Xj0',
        youTubeChannelURL:"https://www.youtube.com/channel/UCskJ0c_kUCeg2IcSD2C1WsQ",
         youTubePlaylistURL:"https://www.youtube.com/channel/UCskJ0c_kUCeg2IcSD2C1WsQ",
        youmaxDefaultTab:"Uploads",
         youmaxColumns:2,
        showVideoInLightbox:false,
         maxResults:6 ,
        });
    </script>
        </div>
<!--Side Menu-->

        <div class="col-lg-3 offset-lg-1">
            <div class="card text-white" style="background-color: #02012a;" data-aos="fade-up"
    data-aos-delay="50"
    data-aos-duration="2000"
    data-aos-mirror="true"
    data-aos-anchor-placement="top-center" >
                <div class="card-header">
                        <h5 class="card-title text-center">
                            Berita Terbaru
                        </h5>
                </div>
             
                <div class="card-body" style="background-color:#ededed">
                    <p class="card-text" style="font-family: 'Rajdhani', sans-serif; text-align: left;">
                                <?php
                                $data = $this->db->query("SELECT * FROM foto_kegiatan WHERE status LIKE '%Yes%' ORDER BY id_ft_kegiatan DESC Limit 4") ;
                                foreach ($data->result_array() as $row) :
                                $id_view=$row['id_ft_kegiatan'];
                                $judul=$row['jdl_kegiatan'];
                                ?>
                                <p class="contoh-link"><a class="contoh-link" href="<?php echo base_url().'berita/view/'.$id_view;?>"> <?php echo $judul; ?></a></p>
                                <?php endforeach;?>
                                </p>
                </div>
                <div class="card-footer">
                    <center><a href="<?php echo base_url().'berita/'?>" class="btn btn-sm btn-warning"> Lihat Berita Lainnya</a></center>
                  </div>
            </div>
        </div>
        </div>
    </div>
<br>
</div>