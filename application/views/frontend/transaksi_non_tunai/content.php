<style type="text/css">
    /* The Modal (background) */
.modal {
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 1; /* Sit on top */
  padding-top: 100px; /* Location of the box */
  left: 0;
  top: 0;
  width: 100%; /* Full width */
  height: 100%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
}

/* Modal Content (image) */
.modal-content {
  margin: auto;
  display: block;
  width: 80%;
  max-width: 700px;
}

/* Add Animation */
.modal-content, #caption {  
  -webkit-animation-name: zoom;
  -webkit-animation-duration: 0.6s;
  animation-name: zoom;
  animation-duration: 0.6s;
}

</style>


<div style="background: url(<?php echo base_url('assets/images/background.jpg'); ?>);background-repeat: repeat;background-size:100% auto;">

<br>
<br>
<br>
<br>
<center>
    <h2 class="container" data-aos="zoom-in-down"
    data-aos-delay="50"
    data-aos-duration="500"
    data-aos-mirror="true"
    data-aos-anchor-placement="top-center" ><b style="font-family: 'Russo One';">RAKOR Transaksi <span style="font-family: 'Russo One';font-size: 40px;color:#ff7700">Non-Tunai</span></b></h2><br>
</center>
<div class="container" data-aos="fade-in"
    data-aos-delay="50"
    data-aos-duration="2000"
    data-aos-mirror="true"
    data-aos-anchor-placement="top-center">


	<div class="card bg-light text-dark">

        <div class="card-body" >
           <!-- <a href="#" data-toggle="modal" class="color-gray-darker c6 td-hover-none"> -->
    


<div class="row">

      <div class="col-sm" style="background-color: white;"> <center style="font-family: 'Russo One';"><h2><b>Pelaksanaan Transaksi Non Tunai</b></h2></center>
        <center style="font-family: 'Russo One';"><h2><b>di Pemerintahan kota Batam</b></h2></center>
        <h3>Dasar : </h3>
        <ol>
            <li>
                <b>Instruksi Presiden Nomor 10 Tahun 2016 Tentang Aksi Pencegahan dan Pemberantasan Korupsi Tahun 2016 dan Tahun 2017;</b>
            </li>
            <li>
                <b>Surat Edaran Menteri Dalam Negeri Nomor 910/1867/SJ Tanggal 17 April 2017</b>
                <ul><li style="margin-left: -21px">Transaksi Non Tunai pada Pemerintah Daerah dilaksanakan paling lambat Tanggal 1 Januari 2018</li></ul>
            </li>
            <li>
                <b>Instruksi Walikota Batam Nomor 23 Tahun 2017 Tanggal 31 Mei 2017 Tentang Pelaksanaan Transaksi Non Tunai;</b>
                <ul><li style="margin-left: -21px">Pelaksanaan TNT dilaksanakan secara bertahap</li>
                    <li style="margin-left: -21px">Tahap I terhitung 1 Juni 2017 ada 6 OPD (Dinkes, Bappelitbang, BPKAD, RSUD, Dinsos, Disdik)</li>
                    <li style="margin-left: -21px">Tahap II terhitung September 2017 dilaksanakan semua OPD</li>
                </ul>
            </li>
            <li>
                <b>Surat Edaran Kepala BPKAD Nomor 279/BPKAD/VI/2017 Tanggal 2 Juni 2017 Tentang Pedoman Umum Pelaksanaan Transaksi Non Tunai di Lingkungan Pemerintah Kota Batam</b>
            </li>
            <li>
                <b>Pengecualian didalam pelaksanaan Transaksi Non Tunai, apabila mendapat persetujuan Kuasa Bendahara Umum</b>
            </li>
        </ol>

        <br>

        <div class="col-sm" style="background: url(<?php echo base_url('assets/images/BGTNT.png')?>"><center style="font-family: 'Russo One';"><h2>LAPORAN PANITIA</h2></center>
        <center style="font-family: 'Russo One';"><h3><b>RAPAT KOORDINASI DAN EVALUASI IMPLEMENTASI TRANSAKSI NON TUNAI</b></h3></center>
        <center style="font-family: 'Russo One';"><h4><b>Se-Indonesia oleh Kemendagri di Batam</b></h4></center>
        <center style="font-family: 'Russo One';"><h4><b>TANGGAL 18 JUNI 2015</b></h4></center>

        <br>

        <h5><b>A. DASAR PENYELENGGARAAN : </b></h5>
        <ol>
            <li>
                Undang-undang Republik Indonesia Nomor 23 Tahun 2014 Tentang Pemerintah Daerah;
            <li>
                Instruksi Presiden Republik Indonesia Nomor 10 Tahun 2016 tentang Aksi Pencegahan dan Pemberantasan Korupsi Tahun 2016 dan 2017;
            </li>
            <li>
                Surat Edaran Menteri Dalam Negeri Republik Nomor 910/1867/SJ tanggal 17 April 2017 tentang Implementasi Transaksi Non Tunai pada Pemerintah Daerah Kabupaten/Kota.
            </li>
        </ol>

        <h5><b>B. MAKSUD DAN TUJUAN</b></h5>
        <p style="margin: 0px 50px 20px 25px;">Rapat Koordinasi dan Evaluasi Implementasi Transaksi Non Tunai ini dimaksudkan untuk saling berbagi pengalaman dalam pelaksanaan transaksi non tunai dan mengetahui kendala-kendala yang dihadapi serta alternatif solusinya di masing-masing pemerintah daerah.
        <br>
        <br>
        Tujuan penyelenggaraan adalah mendorong para stakeholder terkait agar mensosialisasikan ke masyarakat dalam bertransaksi dilakukan secara non tunai di kantor, pusat perbelanjaan, hiburan, perhotelan, restoran, perparkiran dan lain sebagainya.
        </p>

        <h5><b>C. WAKTU PENYELENGGARAAN</b></h5>
        <p style="margin: 0px 50px 20px 25px;">Kegiatan Rapat Koordinasi dan Evaluasi Implementasi Transaksi Non Tunai ini diselenggarakan satu hari pada tanggal 18 Juli 2019.</p>
        
        <h5><b>D. PESERTA</b></h5>
        <p style="margin: 0px 50px 20px 25px;">Peserta berasal dari seluruh Kepala BPKAD, Kepala Bidang Perbendaharaan BPKAD Propinsi, Kabupaten/Kota seluruh Indonesia dan Direksi Bank Pembangunan Daerah seluruh Indonesia sebanyak 1000 orang.</p>
        
        <h5><b>E. NARASUMBER</b></h5>
        <p style="margin: 0px 50px 20px 23px;">
            Narasumber atau pemateri berasal dari :
                <br>
                1. Direktorat Jenderal Bina Keuangan Daerah Kementerian Dalam Negeri Republik Indonesia.
                <br>
                2. Bank Indonesia Propinsi Kepulauan Riau.
                <br>
                3. Otoritas Jasa Keuangan Propinsi Kepulauan Riau.
                <br>
                4. Bank RiauKepri.
                <br>
                5. Perwakilan Bank Pembangunan Daerah.
                </p>
        </ol>

        <h5><b>F. TEMPAT PENYELENGGARAAN</b></h5>
        <p style="margin: 0px 50px 20px 22px;">Tempat Penyelenggaraan adalah bertempat di Hotel Best Western Premier Panbil Kota Batam.</p>

        <h5><b>G. SUMBER DANA</b></h5>
        <p style="margin: 0px 50px 20px 26px;">Sumber Dana pelaksanaan Rapat Koordinasi dan Evaluasi Implementasi Transaksi Non Tunai ini didukung oleh PT. Bank Riau Kepri dan PT. Bank Jabar Banten Cabang Batam.</p>

        <h5><b>H. PENUTUP</b></h5>
        <b><p style="margin: 0px 50px 20px 27px;font-family: 'Bad Script', cursive;">Pulau setokok tempat berasal<br>Pergi ke jodoh membeli bingkai<br>Agar terhindar transaksi ilegal<br>Mari giatkan transaksi non tunai<br><br> Jalan-jalan ke Kampung Patam<br>Singgah sebentar di Kampung Pelita<br>Mari kita jaga Kota Batam<br>Karena Batam Milik Kita</p></b>
      </div>
     

    <!--POTO-->

    <div class="row">

      <div class="col-sm" style="background-color: white;"><center><img style="max-width: 100%; " class="img-fluid img-thumbnail" src="<?php echo base_url()?>assets/images/nontunai/20.jpeg"></center></div>
      <div class="col-sm" style="background-color: white;"><center><img style="max-width: 100%; " class="img-fluid img-thumbnail" src="<?php echo base_url()?>assets/images/nontunai/21.jpeg"></center></div>
      <div class="col-sm" style="background-color: white;"><center><img style="max-width: 100%; " class="img-fluid img-thumbnail" src="<?php echo base_url()?>assets/images/nontunai/22.jpeg"></center></div>
    </div>

   <!--  <br>
      
      </div> -->

      <br>

    <div class="row">

      <div class="col-sm" style="background-color: white;"><img id="myImg1" style="max-width: 100%; " class="img-fluid img-thumbnail" data-u="image" src="<?php echo base_url()?>assets/images/nontunai/1.jpeg"></div>
      <div class="col-sm" style="background-color: white;"><img style="max-width: 100%; " class="img-fluid img-thumbnail" src="<?php echo base_url()?>assets/images/nontunai/2.jpeg"></div>
      <div class="col-sm" style="background-color: white;"><img style="max-width: 100%; " class="img-fluid img-thumbnail" src="<?php echo base_url()?>assets/images/nontunai/6.jpeg"></div>
      
      </div>

      <br>

    <div class="row">
      <div class="col-sm" style="background-color: white;"><img style="max-width: 100%; " class="img-fluid img-thumbnail" src="<?php echo base_url()?>assets/images/nontunai/9.jpeg"></div>
      <div class="col-sm" style="background-color: white;"><img style="max-width: 100%; " class="img-fluid img-thumbnail" src="<?php echo base_url()?>assets/images/nontunai/8.jpeg"></div>
      <div class="col-sm" style="background-color: white;"><img style="max-width: 100%; " class="img-fluid img-thumbnail" src="<?php echo base_url()?>assets/images/nontunai/10.jpeg"></div>
      
    </div>

    <br>

    <div class="row">

      <div class="col-sm" style="background-color: white;"><img style="max-width: 100%; " class="img-fluid img-thumbnail" src="<?php echo base_url()?>assets/images/nontunai/7.jpeg"></div>
      <div class="col-sm" style="background-color: white;"><center><img style="max-width: 100%; " class="img-fluid img-thumbnail" src="<?php echo base_url()?>assets/images/nontunai/13.jpeg"></center></div>
      </div>
  

    <br>

    <div class="row">      
      <div class="col-sm" style="background-color: white;"><center><img style="max-width: 100%; " class="img-fluid img-thumbnail" src="<?php echo base_url()?>assets/images/nontunai/14.jpeg"></center></div>
      <div class="col-sm" style="background-color: white;"><img style="max-width: 100%; " class="img-fluid img-thumbnail" src="<?php echo base_url()?>assets/images/nontunai/11.jpeg"></div>

    </div>

    <br>

    <div class="row">

      <div class="col-sm" style="background-color: white;"><center><img id="myImg" style="max-width: 100%; " class="img-fluid img-thumbnail" data-u="image" src="<?php echo base_url()?>assets/images/nontunai/23.jpeg"></center></div>

    </div>
      

    <br>

     <div class="row">

      <div class="col-sm" style="background-color: white;"><center><img style="max-width: 100%; " class="img-fluid img-thumbnail" src="<?php echo base_url()?>assets/images/nontunai/12.jpeg"></center>
      </div>
    </div>

    <div class="row">
      <div class="col-sm" style="background-color: white;"><center><img style="max-width: 100%; " class="img-fluid img-thumbnail" src="<?php echo base_url()?>assets/images/nontunai/38.jpg"></center></div>
        
  </div>
  <div class="row">

  <div class="col-sm" style="background-color: white;"><center><img style="max-width: 100%; " class="img-fluid img-thumbnail" src="<?php echo base_url()?>assets/images/nontunai/30.jpg"></center>
      </div>
      <div class="col-sm" style="background-color: white;"><center><img style="max-width: 100%; " class="img-fluid img-thumbnail" src="<?php echo base_url()?>assets/images/nontunai/31.jpg"></center>
    </div>
  </div>

  <div class="row">
    <div class="col-sm" style="background-color: white;"><center><img style="max-width: 100%; " class="img-fluid img-thumbnail" src="<?php echo base_url()?>assets/images/nontunai/32.jpg"></center>
    </div>

    <div class="col-sm" style="background-color: white;"><center><img style="max-width: 100%; " class="img-fluid img-thumbnail" src="<?php echo base_url()?>assets/images/nontunai/33.jpg"></center>
    </div>
    </div>

    <div class="row">
       <div class="col-sm" style="background-color: white;"><center><img style="max-width: 100%; " class="img-fluid img-thumbnail" src="<?php echo base_url()?>assets/images/nontunai/34.jpg"></center>
    </div>

    <div class="col-sm" style="background-color: white;"><center><img style="max-width: 100%; " class="img-fluid img-thumbnail" src="<?php echo base_url()?>assets/images/nontunai/35.jpg"></center>
    </div>

  </div>

  <div class="row">
    <div class="col-sm" style="background-color: white;"><center><img style="max-width: 100%; " class="img-fluid img-thumbnail" src="<?php echo base_url()?>assets/images/nontunai/36.jpg"></center>
    </div>

    <div class="col-sm" style="background-color: white;"><center><img style="max-width: 100%; " class="img-fluid img-thumbnail" src="<?php echo base_url()?>assets/images/nontunai/37.jpg"></center>
    </div>
  </div>


    <br>
    </div>
    </div>

    <!-- -->
  </div>
</div>
<br>
<br>
    
</div>

