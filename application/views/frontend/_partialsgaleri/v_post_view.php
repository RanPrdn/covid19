<style>
body {font-family: Arial, Helvetica, sans-serif;}

#myImg {
  border-radius: 5px;
  cursor: pointer;
  transition: 0.3s;
}

#myImg:hover {opacity: 0.7;}

#myImg2 {
  border-radius: 5px;
  cursor: pointer;
  transition: 0.3s;
}

#myImg2:hover {opacity: 0.7;}

#myImg3 {
  border-radius: 5px;
  cursor: pointer;
  transition: 0.3s;
}

#myImg3:hover {opacity: 0.7;}

/* The Modal (background) */
.modal {
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 1; /* Sit on top */
  padding-top: 100px; /* Location of the box */
  left: 0;
  top: 0;
  width: 100%; /* Full width */
  height: 100%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
}

/* Modal Content (image) */
.modal-content {
  margin: auto;
  display: block;
  width: 80%;
  max-width: 700px;
}

/* Add Animation */
.modal-content, #caption {  
  -webkit-animation-name: zoom;
  -webkit-animation-duration: 0.6s;
  animation-name: zoom;
  animation-duration: 0.6s;
}

@-webkit-keyframes zoom {
  from {-webkit-transform:scale(0)} 
  to {-webkit-transform:scale(1)}
}

@keyframes zoom {
  from {transform:scale(0)} 
  to {transform:scale(1)}
}

/* The Close Button */
.close {
  position: absolute;
  top: 15px;
  right: 35px;
  color: #f1f1f1;
  font-size: 40px;
  font-weight: bold;
  transition: 0.3s;
}

.close:hover,
.close:focus {
  color: #bbb;
  text-decoration: none;
  cursor: pointer;
}

.close1 {
  position: absolute;
  top: 15px;
  right: 35px;
  color: #f1f1f1;
  font-size: 40px;
  font-weight: bold;
  transition: 0.3s;
}

.close1:hover,
.close1:focus {
  color: #bbb;
  text-decoration: none;
  cursor: pointer;
}

.close2 {
  position: absolute;
  top: 15px;
  right: 35px;
  color: #f1f1f1;
  font-size: 40px;
  font-weight: bold;
  transition: 0.3s;
}

.close2:hover,
.close2:focus {
  color: #bbb;
  text-decoration: none;
  cursor: pointer;
}

/* 100% Image Width on Smaller Screens */
@media only screen and (max-width: 700px){
  .modal-content {
    width: 100%;
  }
}
</style>

<?php 
	$b=$data->row_array();
?>

	<title><?php echo $b['jdl_kegiatan'];?></title>

	
<style type="text/css">


	  /* Make the image fully responsive */
  .carousel-inner img {
      width: 100%;
      height: 100%;

  }
  
.contoh-link:link,
.contoh-link:active,
.contoh-link:visited{

	color: #000;
	font-size: 12pt;
	text-decoration: none;
	font-family: sans-serif;	
	font-family: 'Roboto Condensed', sans-serif; text-align: left;
}
 
.contoh-link:hover{
	background-color: grey;
}
</style>

	<?php 
		$this->load->view('frontend/_partialsmycss/head.php');
		$this->load->view('frontend/_partialsgaleri/navbar.php');
	?>

  <div style="background: url(<?php echo base_url('assets/images/background.jpg'); ?>);background-repeat: no-repeat, repeat;background-size:100% 100%;">
	<br><br>
	<div class="container">
		<div class="row position" >
		<div class="col-lg-8">
			<h2><?php echo $b['jdl_kegiatan'];?></h2>
			<i>Tanggal Kegiatan : <?php echo date('d-F-Y',strtotime($b['tgl_kegiatan']));?></i><br>
                   <a href="<?php echo base_url().'berita/';?>" class="btn btn-sm btn-warning"> Back</a>
			<hr/>
<!--Slider Gambar-->

<div id="demo" class="carousel slide" data-ride="carousel" >

 
  <!-- The slideshow -->
 <div class="carousel-inner" >
    <div class="carousel-item active" style="cursor:default;position:relative;top:0px;left:0px;width:100%;height:300px;overflow:hidden;">
       <img class="img-fluid" data-u="image" src="<?php echo base_url().'images/'.$b['foto'];?>" />
    </div>
    <div class="carousel-item" style="cursor:default;position:relative;top:0px;left:0px;width:100%;height:300px;overflow:hidden;">
      <img class="img-fluid" data-u="image" src="<?php echo base_url().'images/'.$b['foto2'];?>" />
    </div>
    <div class="carousel-item" style="cursor:default;position:relative;top:0px;left:0px;width:100%;height:300px;overflow:hidden;">
      <img class="img-fluid" data-u="image" src="<?php echo base_url().'images/'.$b['foto3'];?>" />
    </div>  
  </div>
  
  <!-- Left and right controls -->
  <a class="carousel-control-prev" href="#demo" data-slide="prev">
    <span class="carousel-control-prev-icon"></span>
  </a>
  <a class="carousel-control-next" href="#demo" data-slide="next">
    <span class="carousel-control-next-icon"></span>
  </a>
</div>

    <!-- #endregion Jssor Slider End -->
<br>


			<!--KETERANGAN GAMBAR-->
			<p style="text-align: justify;"><?php echo $b['keterangan'];?></p>
			
		</div>

<!--Side Menu-->

		<div class="col-lg-3 offset-lg-1">
			<div class="card text-white" style="background-color: #02012a;" data-aos="fade-up"
    data-aos-delay="50"
    data-aos-duration="2000"
    data-aos-mirror="true"
    data-aos-anchor-placement="top-center" >
				<div class="card-header">
						<h5 class="card-title text-center">
				            Berita Terbaru
				        </h5>
				</div>
			 
				<div class="card-body" style="background-color:#ededed">
					<p class="card-text" style="font-family: 'Rajdhani', sans-serif; text-align: left;">
								<?php
								$data = $this->db->query("SELECT * FROM foto_kegiatan WHERE status LIKE '%Yes%' ORDER BY id_ft_kegiatan DESC Limit 4") ;
								foreach ($data->result_array() as $row) :
								$id_view=$row['id_ft_kegiatan'];
								$judul=$row['jdl_kegiatan'];
								?>
								<p class="contoh-link"><a class="contoh-link" href="<?php echo base_url().'berita/view/'.$id_view;?>"> <?php echo $judul; ?></a></p>
								<?php endforeach;?>
						        </p>
				</div>
				<div class="card-footer">
	                <center><a href="<?php echo base_url().'berita/'?>" class="btn btn-sm btn-warning"> Lihat Berita Lainnya</a></center>
	              </div>
			</div>
		</div>
		</div>
	</div>
<br>


 <div class="container-fluid row" >

    <div class="col-lg-3" data-aos="fade-in"
    data-aos-delay="50"
    data-aos-duration="2000"
    data-aos-mirror="true"
    data-aos-anchor-placement="top-center">
    	<hr/>
       <img id="myImg" class="img-fluid img-thumbnail" data-u="image" src="<?php echo base_url().'images/'.$b['foto'];?>" />
      
    </div>
    <div class="col-lg-3" data-aos="fade-in"
    data-aos-delay="50"
    data-aos-duration="2000"
    data-aos-mirror="true"
    data-aos-anchor-placement="top-center"> 
    	<hr/>
      <img id="myImg1" class="img-fluid img-thumbnail" data-u="image" src="<?php echo base_url().'images/'.$b['foto2'];?>" />
    
    </div>
    <div class="col-lg-3" data-aos="fade-in"
    data-aos-delay="50"
    data-aos-duration="2000"
    data-aos-mirror="true"
    data-aos-anchor-placement="top-center">
    	<hr/>
      <img id="myImg2" class="img-fluid img-thumbnail" data-u="image" src="<?php echo base_url().'images/'.$b['foto3'];?>" />

    </div>

  </div>
  <br><br>
</div>

  <!-- The Modal -->
<div id="myModal" class="modal">
  <span class="close" style="color: white">&times;</span>
  <img class="modal-content" id="img01">
 </div>

   <!-- The Modal -->
<div id="myModal1" class="modal">
  <span class="close1" style="color: white">&times;</span>
  <img class="modal-content" id="img02">
 </div>

   <!-- The Modal -->
<div id="myModal2" class="modal">
  <span class="close2" style="color: white">&times;</span>
  <img class="modal-content" id="img03">
 </div>



<script>
// Get the modal
var modal = document.getElementById("myModal");

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = document.getElementById("myImg");
var modalImg = document.getElementById("img01");
img.onclick = function(){
  modal.style.display = "block";
  modalImg.src = this.src;
  captionText.innerHTML = this.alt;
}

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() { 
  modal.style.display = "none";
}
</script>

<script>
// Get the modal
var modal = document.getElementById("myModal1");

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = document.getElementById("myImg1");
var modalImg = document.getElementById("img02");
img.onclick = function(){
  modal.style.display = "block";
  modalImg.src = this.src;
  captionText.innerHTML = this.alt;
}

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close1")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() { 
  modal.style.display = "none";
}
</script>

<script>
// Get the modal
var modal = document.getElementById("myModal2");

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = document.getElementById("myImg2");
var modalImg = document.getElementById("img03");
img.onclick = function(){
  modal.style.display = "block";
  modalImg.src = this.src;
  captionText.innerHTML = this.alt;
}

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close2")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() { 
  modal.style.display = "none";
}
</script>
<?php   
	$this->load->view('frontend/_partialsmycss/alamat.php');
    $this->load->view('frontend/_partialsmycss/footer.php');
    $this->load->view('frontend/_partialsmyjs/js.php'); 
?>
