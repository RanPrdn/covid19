<!DOCTYPE html>
<html lang="en">
<head>
	  

<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>BPKAD</title>
  <!--Icon-->
  <link rel="shortcut icon" href="<?php echo base_url('assets/images/iconpemko.ico') ?>">
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

	<!-- Animate AOS CSS-->
	<link href="<?php echo base_url('assets/css/aos.css') ?>" rel="stylesheet">

	
	<!--Font Awesome Bootsrap 4 -->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

	<!--Fonts Google-->
	<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed|Bad+Script|Khand|Russo+One|Rajdhani&display=swap" rel="stylesheet">


<!--DataTables -->
  <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">


<!--Youmax Youtube -->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/youmax.min.css') ?>">
<script type="text/javascript" src="<?php echo base_url('assets/js/youmax.min.js') ?>"></script>
<style type="text/css">

 /*jssor slider loading skin spin css*/
        .jssorl-009-spin img {
            animation-name: jssorl-009-spin;
            animation-duration: 1.6s;
            animation-iteration-count: infinite;
            animation-timing-function: linear;
        }

        @keyframes jssorl-009-spin {
            from { transform: rotate(0deg); }
            to { transform: rotate(360deg); }
        }

        .jssora061 {display:block;position:absolute;cursor:pointer;}
        .jssora061 .a {fill:none;stroke:#fff;stroke-width:360;stroke-linecap:round;}
        .jssora061:hover {opacity:.8;}
        .jssora061.jssora061dn {opacity:.5;}
        .jssora061.jssora061ds {opacity:.3;pointer-events:none;}


        
	  /* Make the image fully responsive */
  .carousel-inner img {
      width: 100%;
      height: 100%;

  }
  

	.navbar-custom {
    background-color: #02012a;
	}
	/* change the brand and text color */
	.navbar-custom .navbar-brand,
	.navbar-custom .navbar-text {
	    color: white;
	}
	/* change the link color */
	.navbar-custom .navbar-nav .nav-link {
	    color: white;
	}
	/* change the color of active or hovered links */
	.navbar-custom .nav-item.active .nav-link,
	.navbar-custom .nav-item:hover .nav-link {
	    color: orange;
	}

	/*Medsos*/
	.fa {
	  padding: 20px;
	  font-size: 30px;
	  
	}

	.fa:hover {
	    opacity: 0.7;
	    color: #fcc203
	}

	.fa-facebook {
	  
	  color: white;
	}

	.fa-google {
	
	  color: white;
	}
	.fa-instagram {
	
	  color: white;
	}

	.back-to-top {
    position: fixed;
    bottom: 25px;
    right: 25px;
    display: none;
}
    /*Card deck*/

@media (min-width: 576px)
.card-deck {
    -ms-flex-flow: row wrap;
    flex-flow: row wrap;
    /* margin-right: -15px; */
    /* margin-left: -15px; */
}
.card-deck {
    display: -ms-flexbox;
    display: flex;
    -ms-flex-direction: column;
    flex-direction: column;
}
.container-fluid {
    width: 100%;
    /* padding-right: 15px; */
    /* padding-left: 15px; */
    margin-right: auto;
    margin-left: auto;
}

.dropdown:hover>.dropdown-menu {
	background-color: #f0f0f0;
  display: block;
}

.dropdown>.dropdown-toggle:active {
  /*Without this, clicking will make it sticky*/
    pointer-events: none;
}

.dropdown:hover>.dropdown-menu {

  display: block;
}

.dropdown>.dropdown-toggle:active {
  /*Without this, clicking will make it sticky*/
    pointer-events: none;
}

.dropdown-menu {
    background-color: #f2f2f2;
}

.dropdown-menu a:hover {
    background-color: orange;
}


.dropdown-submenu {
  position: relative;
}

.dropdown-submenu a::after {
  transform: rotate(-90deg);
  position: absolute;
  right: 6px;
  top: .8em;
}

.dropdown-submenu .dropdown-menu {
  top: 0;
  left: 100%;
  margin-left: .1rem;
  margin-right: .1rem;
}


h4 { font-family: 'Open Sans'; margin: 0;}

.modal,body.modal-open {
    padding-right: 0!important
}

body.modal-open {
    overflow: auto
}

body.scrollable {
    overflow-y: auto
}

.modal-footer {
	display: flex;
	justify-content: flex-start;
	.btn {
		position: absolute;
		right: 10px;
	}
}




@media screen and (max-width: 600px) {

  /* USE THESE CLASSES TO HIDE CONTENT ON MOBILE */
 .mobile-hide {
    display: none !important;
 }

}


@media screen and (min-width: 600px) {

  /* USE THESE CLASSES TO HIDE CONTENT ON MOBILE */
 .desktop-hide {
    display: none !important;
 }

}


  .kotak
 {
  padding:10px;
  border:1px solid #e8e8e8;
  margin-bottom:15px;
  background:#F4F4F4;
  border-radius:5px;
 }
</style>
</head>
