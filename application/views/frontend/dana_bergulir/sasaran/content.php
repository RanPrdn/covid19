<div style="background: url(<?php echo base_url('assets/images/background.jpg'); ?>);background-repeat: no-repeat, repeat;background-size:cover;">

<br>
<br>
<br>
<br>
<center>
    <h2 class="container" data-aos="fade-in"
    data-aos-delay="50"
    data-aos-duration="500"
    data-aos-mirror="true"
    data-aos-anchor-placement="top-center" style="font-family: 'Russo One'; font-size: 40px;color:#ff7700">SASARAN <span style="font-family: 'Russo One';font-size: 35px;color:#000">PINJAMAN DANA BERGULIR</span><br></h2>
</center>
<div class="container" data-aos="fade-in"
    data-aos-delay="50"
    data-aos-duration="2000"
    data-aos-mirror="true"
    data-aos-anchor-placement="top-center">

    <div class="card bg-light text-dark">
        <div class="card-body">
            <ol type="1">
            <li>Koperasi yang berbadan hukum.</li>
            <li>Usaha mikro yang ada di daerah. dan</li>
            <li>Usaha mikro memiliki kriteria sebagai berikut :
                <ol type="a">
                    <li style="margin-left: -24px">Handy craft (kerajinan tangan).</li>
                    <li style="margin-left: -24px">Home industri (industri rumah tangga).</li>
                    <li style="margin-left: -24px">Sektor perdagangan.</li>
                    <li style="margin-left: -24px">Aneka usaha yang produktif dan</li>
                    <li style="margin-left: -24px">Usaha jasa.</li>
                </ol>
            </li>
          </ol>
          
    </div>
</div>
<br>
<br>
</div>
</div>