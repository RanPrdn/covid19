<div style="background: url(<?php echo base_url('assets/images/background.jpg'); ?>);background-repeat: no-repeat, repeat;background-size:cover;">

<br>
<br>
<br>
<br>
<center>
    <h2 class="container" data-aos="fade-in"
    data-aos-delay="50"
    data-aos-duration="500"
    data-aos-mirror="true"
    data-aos-anchor-placement="top-center" style="font-family: 'Russo One';color:#000">JAMINAN<br></h2>
</center>
<div class="container" data-aos="fade-in"
    data-aos-delay="50"
    data-aos-duration="2000"
    data-aos-mirror="true"
    data-aos-anchor-placement="top-center">

    <div class="card bg-light text-dark" style="font-family: Roboto">
        <div class="card-body">
          <ol type="1">
          <li>Setiap pemohon yang mengajukan pinjaman dana bergulir wajib menyertakan jaminan atas nama pemohon dan atau suami atau isteri pemohon.</li>
          <li>Jaminan atas nama suami atau isteri pemohon, wajib melampirkan surat kuasa dan surat persetujuan dari pemilik jaminan dengan materai dan disaksikan sekurang-kurangnya 2 (dua) orang.</li>
          <li>Jaminan yang diberikan pemohon harus berada di wilayah Kota Batam.</li>
          <li>Melampirkan Akta Kuasa Menjual dari pemohon/Pemilik Jaminan yang dibuat dihadapan Notaris.</li>
          <li>Jaminan yang diberikan oleh pemohon dapat berupa :</li>
            <ol type="a">
            <li style="margin-left: -24px">Tanah/bangunan (Sertifikat Kepemilikan/Surat Kepemilikan yang sah dikeluarkan oleh pihak yang berwenang), melampirkan Foto copy bukti pembayaran pajak bumi dan bangunan (PBB) tahun terakhir.</li>
          </ol>
          <li>Jaminan asli diserahkan pada saat penandatanganan akad kredit pinjaman.</li>
          <li>UPT-PDB wajib menyimpan dan memeilihara jaminan asli sehingga tetap dalam keadaan baik dan terawat.</li>
          <li>Jaminan asli dikembalikan kepada penerima pinjaman setelah pokok, bunga/jasa dan denda pinjaman dibayarkan lunas.</li>
          </ol>
        </div>
    </div>
<br>
<br>
    
</div>