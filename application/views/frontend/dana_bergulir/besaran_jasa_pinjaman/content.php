<div style="background: url(<?php echo base_url('assets/images/background.jpg'); ?>);background-repeat: no-repeat, repeat;background-size:cover;">

<br>
<br>
<br>
<br>
<center>
    <h2 class="container" data-aos="fade-in"
    data-aos-delay="50"
    data-aos-duration="500"
    data-aos-mirror="true"
    data-aos-anchor-placement="top-center" style="font-family: 'Russo One'; font-size: 40px;color:#ff7700">BESARAN JASA <span style="font-family: 'Russo One';font-size: 35px;color:#000">PINJAMAN</span><br></h2>
</center>
<div class="container" data-aos="fade-in"
    data-aos-delay="50"
    data-aos-duration="2000"
    data-aos-mirror="true"
    data-aos-anchor-placement="top-center">

    <div class="card bg-light text-dark" style="font-family: Roboto">
        <div class="card-body">
            <ol type="1">
            <li>Penerima pinjaman dikenakan bunga/jasa pinjaman sebesar 6% (enam persen) flat pertahun;</li>
            <li>Pada saat pencairan pinjaman, penerima pinjaman wajib membayar angsuran pertama pokok dan bunga/jasa pinjaman.</li>
            <li>Penerima pinjaman wajib membayar angsuran pinjaman ke Bank yang ditunjuk menggunakan slip pembayaran pokok dan slip penyetoran bunga/jasa dibuat secara terpisah, dengan rincian sebagai berikut : 
                <ol type="a">
                <li style="margin-left: -24px">Setoran pokok nomer rekening : <b>106.02.02000</b> atas nama penerima pokok UPT dana bergulir dan</li>
                <li style="margin-left: -24px">Setoran bunga/jasa nomor rekening : <b>106.02.04000</b> atas nama penerima bunga/jasa UPT dana bergulir.</li>
            </li>
            <li style="margin-left: -24px">Pengembalian pokok pinjaman dan bunga/jasa dilakukan sesuai dengan perjanjian pinjaman.</li>
            <li style="margin-left: -24px">Penerima pinjaman yang melakukan pelunasan dipercepat wajib membayar sisa pokok dan bunga/jasa pinjaman pada bulan berjalan.</li>
          
        </div>
    </div>
<br>
<br>
</div>
</div>