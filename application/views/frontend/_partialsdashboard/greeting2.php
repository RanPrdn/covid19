
<style type="text/css">
 /* Flashing */
.hover13 figure:hover img {
  opacity: 1;
  -webkit-animation: flash 1.5s;
  animation: flash 1.5s;
}
@-webkit-keyframes flash {
  0% {
    opacity: .4;
  }
  100% {
    opacity: 1;
  }
}
@keyframes flash {
  0% {
    opacity: .4;
  }
  100% {
    opacity: 1;
  }
}
</style>

<div style="background-color:#ff6600;color: white; font-family: 'Roboto Condensed', sans-serif;" >
	<div class="container-fluid" >
    <br><br>
    <h1 style="font-family: 'Roboto Condensed', sans-serif;"><center>KUNJUNGI APLIKASI KAMI</center></h1><br>
    <div class="row container-fluid hover13">
    <div class="col-lg-2 col-md-2 offset-lg-1" data-aos="flip-left"
  	data-aos-offset="10"
    data-aos-delay="50"
    data-aos-duration="1000"
    data-aos-mirror="true"
    data-aos-anchor-placement="top-center" >
   <center><figure><a href="http://bmd-kemendagri.id/" target="_blank"><img class="img-fluid rounded" style="max-width: 50%" src="<?php echo base_url('assets/images/a1.png'); ?>"></a></figure>Sistem Informasi Administrasi Pengelolaan Barang Milik Daerah</center></div>
   <div class="col-lg-2 col-md-2" data-aos="flip-left"
  	data-aos-offset="10"
    data-aos-delay="150"
    data-aos-duration="1000"
    data-aos-mirror="true"
    data-aos-anchor-placement="top-center" >
 <center> <figure><img class="img-fluid rounded" style="max-width: 50%" src="<?php echo base_url('assets/images/a2.png'); ?>"></figure>Sistem Informasi Gaji</center>
</div> 
<div class="col-lg-2 col-md-2" data-aos="flip-left"
  	data-aos-offset="10"
    data-aos-delay="200"
    data-aos-duration="1000"
    data-aos-mirror="true"
    data-aos-anchor-placement="top-center" >
 <center><figure><img class="img-fluid rounded" style="max-width: 50%" src="<?php echo base_url('assets/images/a3.png'); ?>"></figure>Sistem Informasi Barang Persediaan </center>
</div>
<div class="col-lg-2 col-md-2" data-aos="flip-left"
  	data-aos-offset="10"
    data-aos-delay="250"
    data-aos-duration="1000"
    data-aos-mirror="true"
    data-aos-anchor-placement="top-center">
  <center> <figure><img class="img-fluid rounded" style="max-width: 50%" src="<?php echo base_url('assets/images/a4.png'); ?>"><br><br>Elektronik Filling </figure> </center>
</div>
<div class="col-lg-2 col-md-2" data-aos="flip-left"
    data-aos-offset="10"
    data-aos-delay="250"
    data-aos-duration="1000"
    data-aos-mirror="true"
    data-aos-anchor-placement="top-center">
  <center> <figure><a href="http://sipkd.batam.go.id/" target="_blank"><img class="img-fluid rounded" style="max-width: 50%" src="<?php echo base_url('assets/images/a5.png'); ?>"></a></figure> Sistem Pengelolaan Keuangan Daerah <br>(*Internet Explorer Only) </center>
</div>
	</div>
	<br><br>
</div>
</div>
