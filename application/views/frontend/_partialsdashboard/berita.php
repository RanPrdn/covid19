<style type="text/css">


.thumbnail {
  position: relative;
  width: 100%;
  height:350px;
  overflow: hidden;
}
.thumbnail img {
  position: absolute;
  left: 50%;
  top: 50%;
  height: 100%;
  width: auto;
  -webkit-transform: translate(-50%,-50%);
      -ms-transform: translate(-50%,-50%);
          transform: translate(-50%,-50%);
}
.thumbnail img.portrait {
  width: 100%;
  height: auto;
}

.zoom {
  padding: -1px;
  transition: transform .5s;
  margin: 0 auto;
}

.zoom:hover {
  -ms-transform: scale(1.1); /* IE 9 */
  -webkit-transform: scale(1.1); /* Safari 3-8 */
  transform: scale(1.1); 
}

</style>
<!--BERITA TERKINI START-->
<div  style="background-color: #ffffff;">
	<br><br>

   <div class="container" style="font-family: 'Roboto Condensed', sans-serif;">
    <center><h2><b>GALERI KEGIATAN</b></h2></center><br>
      <div class="row">
      <?php
        function limit_words($string, $word_limit){
                  $words = explode(" ",$string);
                  return implode(" ",array_splice($words,0,$word_limit));
              }
        foreach ($data->result_array() as $i) :
          $id=$i['id_ft_kegiatan'];
          $judul=$i['jdl_kegiatan'];
          $image=$i['foto'];
          $isi=$i['keterangan'];
      ?>
      <div class="col-lg-3">
              <div class="kotak zoom">
              <div class="thumbnail" data-aos="fade-in"
    data-aos-delay="50"
    data-aos-duration="2000"
    data-aos-mirror="true"
    data-aos-anchor-placement="top-center"><a href="<?php echo base_url().'index.php/berita/view/'.$id;?>"><img class="portrait" src="<?php echo base_url().'images/'.$image;?>"/></a></div>
                <div class="card-body" style="height: 300px">
                  <h5 class="card-title" style="color:#3a5c91">
                    <a href="<?php echo base_url().'berita/view/'.$id;?>"><b><?php echo $judul; ?></b></a>
                  </h5>
                    <p class="card-text"><?php echo limit_words($isi,10)." ...";?></p>
               </div>
                <div class="card-footer">
                  <a href="<?php echo base_url().'berita/view/'.$id;?>" class="btn btn-sm btn-success"> Selengkapnya</a>
                </div>
              </div>
          </div>
      <?php endforeach;?>
    </div>
  </div>
</div>
<br>
<br>
</div>
<!--BERITA TERKINI END -->