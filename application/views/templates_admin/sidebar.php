     <div id="sidebar" class="sidebar                  responsive                    ace-save-state">
        <script type="text/javascript">
          try{ace.settings.loadState('sidebar')}catch(e){}
        </script>

        <ul class="nav nav-list">
          <li class="active">
            <a href="<?php echo base_url('admin/dashboard_admin')?>">
              <i class="menu-icon fa fa-tachometer"></i>
              <span class="menu-text"> Dashboard </span>
            </a>

            <b class="arrow"></b>
          </li>

          <li class="">
            <a href="<?php echo base_url('admin/data_pemberi')?>">
              <i class="menu-icon fa fa-desktop"></i>
              <span class="menu-text">Terima Barang</span>
            </a>

            <b class="arrow"></b>
          </li>

          <li class="">
            <a href="<?php echo base_url('admin/data_penyalur')?>">
              <i class="menu-icon fa fa-desktop"></i>
              <span class="menu-text">Penyerahan Barang</span>
            </a>

            <b class="arrow"></b>
          </li>

          <li class="">
            <a href="<?php echo base_url('admin/laporan_bast')?>">
              <i class="menu-icon fa fa-print"></i>
              <span class="menu-text">BAST</span>
            </a>

            <b class="arrow"></b>
          </li>

        </ul><!-- /.nav-list -->

        <div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
          <i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
        </div>
      </div>