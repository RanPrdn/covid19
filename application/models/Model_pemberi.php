<?php

class Model_pemberi extends CI_Model
{
	
	public function tampil_data(){
		return $this->db->get('terima');
	}

	public function tambah_beri($data, $table){
		$this->db->insert($table, $data);
	}

	public function edit_terima($where, $table){
		return $this->db->get_where($table, $where);
	}

	public function update_data($where, $data, $table)
	{
		$this->db->where($where);
		$this->db->update($table, $data);
	}

	public function hapus_data($where, $table)
	{
		$this->db->where($where);
		$this->db->delete($table);
	}
}
?>