<?php

class Model_penyalur extends CI_Model
{
	
	public function tampil_data(){
		return $this->db->query('SELECT penyaluran.id_penyaluran, penyaluran.tgl_penyaluran, penyaluran.skpd, penyaluran.penerima, penyaluran.nama_barang, penyaluran.jumlah, penyaluran.satuan_salur,penyaluran.ket, terima.nama_pemberi FROM penyaluran, terima WHERE penyaluran.id_terima=terima.id_terima');
	}


	function cari($id){
        $query= $this->db->get_where('terima',array('nama_pemberi'=>$id));
        return $query;
    }

	public function tambah_salur($data, $table){
		$this->db->insert($table, $data);
	}

	public function edit_terima($where, $table){
		return $this->db->get_where($table, $where);
	}

	public function update_data($where, $data, $table)
	{
		$this->db->where($where);
		$this->db->update($table, $data);
	}

	public function hapus_data($where, $table)
	{
		$this->db->where($where);
		$this->db->delete($table);
	}
}
?>