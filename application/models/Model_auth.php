<?php
	class Model_auth extends CI_Model {
		var $CI = NULL;
		 public function __construct()
		 {
			 $this->CI =& get_instance();
		 }
		 // Fungsi login
		 public function login($username, $pwd) 
		 {
			 $query = $this->CI->db->get_where('user',array('username'=>$username,'pwd' => $pwd));
			 if($query->num_rows() == 1) {
			 $row = $this->CI->db->query('SELECT id_user FROM user where username = "'.$username.'"');
			 $admin = $row->row();
			 $id = $admin->id_user;
			 $this->CI->session->set_userdata('username', $username);
			 $this->CI->session->set_userdata('id_login', uniqid(rand()));
			 $this->CI->session->set_userdata('id', $id);

			 redirect(base_url('admin/dashboard_admin'));
		 }else{
			 $this->CI->session->set_flashdata('sukses','Oops... Username/password salah');
			 redirect(base_url('auth/login'));
		 }
		 return false;
		 }
		 // Proteksi halaman
		 public function cek_login() 
		 {
			 if($this->CI->session->userdata('username') == '') {
			 $this->CI->session->set_flashdata('sukses','Anda belum login');
			 redirect(base_url('auth/login'));
		 }
		 }
		 // Fungsi logout
		 public function logout() {
		 $this->CI->session->unset_userdata('username');
		 $this->CI->session->unset_userdata('id_login');
		 $this->CI->session->unset_userdata('id');
		 $this->CI->session->set_flashdata('sukses','Anda berhasil logout');
		 redirect(base_url('auth/login'));
 		}
	}
?>