<?php
	class Model_log_aktivitas extends CI_Model {

		function getAll($limit, $offset){
			$this->db->limit($limit);
			$this->db->offset($offset);
			$this->db->order_by('log_id desc');
			return $this->db->get('tabel_log')->result();
		}
		function countAll(){
			return $this->db->get('tabel_log')->num_rows();
		}
		
	}
?>